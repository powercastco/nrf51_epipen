/*
 * Copyright (c) 2015 Powercast Corporation and (c) 2013 Nordic Semiconductor (see below).
 */
 
/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
 
 /* DOCUMENTATION NOTES:
 *
 * - See Network folder: Edison->shares-> 90 Projects-> Mylan_Truefit_EpiPen
 * - Packet format - see "Epipen Holder Advertising Data Packet Format (V7).docx"
 */


// SEE #define DUAL_SWITCH_EPIPEN_DEVICE 1 in nrf_drv_config.h file - comment out that line to use single switch epiPen
#define USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA 1
#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
		#define IBEACON_EVERY_xTH_BEACON (5)
		//#define ONLY_BUMP_IBEACON_MAJOR_WHEN_FLAGS_CHANGE 1
		#define USE_BACKWARDS_GAP_ADDR_IN_LAST_3_OF_16_IBEACON_UUID_BYTES 1
		#ifndef USE_BACKWARDS_GAP_ADDR_IN_LAST_3_OF_16_IBEACON_UUID_BYTES
				#ifndef USE_LONG_MFG_SPECIFIC_DATA
						//#define USE_16_BYTE_IBEACON_UUID_IN_PC_BEACON_MFG_DATA 1  // Per Truefit, don't use the special PC beacon with ibeacon UUID because their phone app can't receive this beacon as it doesn't have 16- byte ble Flag Service UUID
						#define IBEACON_UUID_IN_PC_BEACON_EVERY_xTH_INIT_TIME  (10) 			
				#endif
		#endif
#endif

#define USE_BUTTON_APP_LIBRARY	1
//#define DONT_START_SOFTDEVICE_SO_CAN_DEBUG_EASIER 1

 #define USE_STARDARD_ERR_HANDLER 1  									// else use one better suited for the IDE Debugger
//#define USE_LONG_MFG_SPECIFIC_DATA 1

//#define REMOVE_PARTIAL_LIST_SERVICES_FROM_ADV_PACKET
#define REMOVE_COMPLETE_LIST_SERVICES_FROM_ADV_PACKET

//#define PROVIDE_SCAN_RESPONSE_DATA_TO_STACK  1      // 

//#define USE_SYSTEM_OFF_MODE_ON_GAP_EVT_TIMEOUT 1		// Else do system reset						
//#define CONVERT_BATTERY_VOLTAGE_TO_PERCENT 1				// Else use volts (e.g., 29 = 2.9V)

// Uncomment #defines below to run test code during in-house testing!
//#define TEST_CODE_PUT_TICK_COUNT_IN_MFG_DEV_TYPE_LSB 1  // helps tester know getting new adv pcks					
//#define TEST_CODE_FORCE_WATCHDOG_TIMEOUT 1
//#define TEST_CODE_FOR_EVENT_COUNTERS 1
//#define DONT_EVER_DO_BAT_N_TEMP_MEASUREMENTS 1  // if uncomment this line - don't do measurements when testing PPI example code because interferes
//#define TEST_CODE_USE_TEMPERATURE_THRESHOLD_COUNT_OF_ONE 1  // normally count of 10 before indicate too low or too high in persistent flags
//#define TEST_CODE_FOR_BATTERY_LOW_FLAG 1
//#define TEST_CODE_FOR_TEMP_AT_LOW_THRESHOLD 1
//#define TEST_CODE_FOR_TEMP_AT_HIGH_THRESHOLD 1
//#define TEST_CODE_FOR_TEMP_TOO_LOW 1
//#define TEST_CODE_FOR_TEMP_TOO_HIGH 1

 
 /* Powercast Source File History/Change log:
 * 
 * Tools Notes:
 * - Softdevice: C:/Keil_v5/ARM/Pack/NordicSemiconductor/nRF_SoftDevice_S110/8.0.3/hex/s110_nrf51_8.0.0_softdevice.hex (SDK 9 & 10 version - SDK11 does not support S110)
 * - Keil: Keil MDK 5.17
 *
 * SMART EpiPen Phase III (add 2nd beacon format to help when iphone app swiped closed): 
 *
 * 		2016-08-15 Mark Brasili/MRB: Comments added on 19th, for code changes dated 8/15/16:
 *               Problems:
 *               (1) When iphone app is asleep, or swiped closed, IOS doesn't provide it the Epipen beacons, so the app doesn't detect pen pulled (e.g., send ibeacon format too)
 *               (2) If user has two Epipen devices, the iphone app needs to know which device's pen is pulled (e.g., send 3 bytes of ble GAP address in each beacon format)
 *               (3) iphone app seeing longer delays than desired (e.g., 5-10 seconds) reporting either pen pulled, or chirping the device (e.g, change advertising rate back to 1 second)
 *               Design/Code Changes:
 *               - Send ibeacon format that IOS will process even when iphone is asleep, in addition 
 *                 to the normal Epipen beacon with flag bit map (pen out/in, temp high/low, battery low)
 *               - Fast advertising rate stays 100 ms for 30 seconds and used when pen pulled or after a reset/battery inserted.
 *               - Normal advertising rate is once per second
 *               - Beacon Format Logic - Every second change beacon format, regardless of the current advertising rate
 *                 Normal beacon format setup once per second for four times then on next second tick setup one iBeacon format , then repeat.  The total cycle is 5 seconds.
 *                 The upper two bits of normal beacon flag field are a second count that is change every time a normal ibeacon format is setup (e.g. 01, 10, 11, 10 for 1, 2 , 3 & 4)
 *                 This incrementing second count was thought that it may help IOS see more normal beacons.  This may not be the case per Truefit, but the logicis still in the s/w.
 *                 iBeacon Major byte 2 field increments every iBeacon setup in the range 2-10.  After a reset, it will start at 3.  That range contains 9 different values.
 *                 It takes 45 seconds for ibeacon major number to wrap and start over (5 seconds * 9);  This helps with IOS timing for ibeacon entry events.
 *                 the iphone app sets up an ibeacon entry event for each of the possible ibeacon major values (1-10).  Truefit can provide more info, but
 *                 it may take up to 30 seocnds for the iphone to detect an entry event - Truefit also tried using ibeacon exit events too.
 *               - Pen removed Logic - immediately change beacon format to an iBeacon, but use reserved ibeacon major byte 2 equal to a "1" value meaning pen pulled.
 *                 Change to fast advertising rate (100 ms) for 30 seconds.
 *                 While usign the fast advertising rate, the iphone will get about 10 ibeacons with major = 1 per second,
*                  after each time setup the ibeacon format.  This makes sure the app wakes up.
 *                 Continue to change the beacon format every second, setting up normal beacons four times before setting up the next ibeacon.
 *                 iBeacon major byte = 1 for 30 seconds to make sure iphone sees it (e.g., major does not increment).  After this 30 seconds, it goes back to incrementing in range 2-10, 
 *                 whether the pen was reinserted or not; This prevents it from waking up the phone app when the app should already knows that the pen was pulled.
 *                 If pen if reinserted and repulled the process starts over (even if reinserted before the 30 second timeout).
 *               - See the separate Epipen Advertising document for the format and data contents of the two beacon formats.  In summary,
 *                 The ibeacon format contains a Proximity UUID field.  The base number for this field was generated by Truefit using a utiltiy app.
 *                 The last three bytes of this UUID are set to the last three bytes of the ble chip's internal 6-byte GAP address, listed in reverse order (byte 6, 5 then 4)
 *                 These same last three bytes are also used in the normal beacon format's Serial Number field.
 *                 Both formats having the same three bytes of address information allows the iphone app to know which Epipen devices it is receving from when it gets an iBeacon
 *                 The phone app can only connect to an Epipen device when it is sending the normal beacon format.  ibeacons are normally not connectable although
 *                 some non Apple test utilities may be able to connect to only the Epipen's ibeacon (e.g., Nordic's Master Control Utility), but IOS may not.
 *               - The iBeacon format contains the Apple company ID (0x004C), the normal beacon format was changed to now contain the Powercast Company ID 0x02D3 (was 0xFFFF)
 *
 *
 * SMART EpiPen Phase II (new h/w with buzzer): 
 *
 * 		2015-09-21 Mark Brasili/MRB: Need to add buzzer support and 128-bit UUID list to advertising data.  New h/w supports buzzer.
 *                     - Hit problems using Silver Lab laptop with nrF51 SDK Ver 6 trying to use PPI feature for buzzer.
 *                     - Decision was made to add Windows VM to my Linux PC and install latest Keil and Nordic SDK (9.0.0)
 *			                 PORT NOTE: The original configuration was nRF51822 xxAA	16MHz	IROM1 0x16000 0x9000  IRAM1 0x20002000 0x2000.
 *			                 Changed to:                          nRF51822 xxAB  16MHz IROM1 0x18000 0x7000  IRAM1 0x20002000 0x2000.  (Newer version of softdevice larger - PORT change)
 *                       PORT NOTE: defined SOFTDEVICE_PRESENT in project options C/C++ tab so builds clean - PORT change
 *                       PORT NOTE: Removed app_gpiote.c and replaced it with nrf_drv_gpiote.c because got link error "Symbol GPIOTE_IRQHandler multiply defined" and new app_button.c makes driver calls.
 *                       PORT NOTE: search on "PORT change" for other SDK 9.0.0 changes (e.g., some APIs changed, etc.)
 *                     - Still problems using PPI (with buzzer GPIO and timer1), so opened ticket with Nordic (case ID 24293)
 *                     - Coded buzzer logic that may be temporary logic (does not use PPI)
 * 		2015-09-21 Mark Brasili/MRB: Add buzzer logic (if App connects and sets chirp flag, device will chirp three times when app disconnects).
 * 		2015-09-21 Mark Brasili/MRB: Add 128-bit UUID to advertising data and removed last 5 bytes of battery and temperature bytes to make room.
 *                     - Will use battery low and termperature low/high bit flags instead.
 *                     - Compiler switches:
 *                       //#define USE_LONG_MFG_SPECIFIC_DATA 1
 *                       //#define REMOVE_PARTIAL_LIST_SERVICES_FROM_ADV_PACKET
 *                       #define REMOVE_COMPLETE_LIST_SERVICES_FROM_ADV_PACKET 
 * 		2015-09-21 Mark Brasili/MRB: Charlie previously said to use internal MAC address for serial number not a unique number progammed into the device's flash.
 *                     - Compiler switch change:
 *                       //#define GET_SERIAL_NUMBER_FROM_UICR_REG_FLASH 1 >>> now using bytes of internal advertising address of nRF51 for serial #
 *    2015-10-13 Mark Brasili/MRB: Got Nordic, nRF51 SDK examples, gpiote (PC) working for PWM (Timer1 events to GPIO for buzzer), so will try code in this project with softdevice:
 *                     - Add nRF_Drivers: nrf_drv_ppi.c & nrf_drv_timer.c to project
 *                     - In project optoins, C/C++ Tab, add path to .h file for above drivers too.
 *                     - Add buzzer PPI code under compiler switch: #define USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9 1
 *                     - If works, code under compiler switch USE_EXAMPLE_CODE_GPIOTE_PPI_SDK6, that doesn't seem to work, will be removed
 *                     - To fix link error: "Error: L6406E: No space in execution regioins with .ANY selector matching ...", I changed Project options on Target tab as follows:
 *                       IROM settings from 0x1800 0x7000 to 0x1800 0x8000 (I think the 0x8000 can get a lot bigger too per memory block diagram in 51 Ref manual (Code space up to FICR area 0x1000 0000)
 *                       IRAM settings from 0x20002000 0x2000 to 0x20002000 0x6000 (I'm not sure this was needed as this alone didn't fix the link error - e.g., was using RAM2 & RAM3 areas now also RAM4-7)
 *    2015-10-13 Mark Brasili/MRB:
 *                     - GPIOTE example not working with softdevice, will try PWM example code found
 *                     - Add nRF_Libraries: app_pwm.c/h
 *                     - In project options, C/C++ Tab add path to app_pwm.h
 *                     - This doesn't work either
 *    2015-10-23 Mark Brasili/MRB:
 *                     - Add support for Dual Epipen (adv device type 0x4532 ("E2" ASCII), 0x20 flag bit now for Pen #2 out of holder
 *                     - initialze Epipen out of holder flags correctly
 *                     - Need to fix issue if pen is in holder when battery inserted, don't get event when pen goes out of holder
 *    2015-10-26 Mark Brasili/MRB:
 *                     - Add gpiote logic for events when switches change state to replace button app library logic that has problem where
 *                       If pen in when battery put in don't get pen out event unless go back in and out again.
 *    2015-10-27 Mark Brasili/MRB:
 *                     - To help with faster App connection times when doing multiple chirps, use faster advertising rate when finished chirping;
 *                       Since reset device when done chirping this means use 100 ms faster advertsing rate at startup time for 30 seconds.
 *                     - Change normal advertising rate from 10.24 seconds to 5 seconds, per CG (maybe just for demo - TBD)
 *                     - Disable interrupts when start using the buzzer.  Maybe softdevice interference.  Maybe related to iWatches ble tx traffic.
 *                       Sometimes sounds not correct.  Sometimes only get 1 or 2 chirps not 3.  Also, don't exit loop early if App connects.
 *    2015-10-27 Mark Brasili/MRB:
 *                     - Do some code clean-up - as much as can get done in a limited time.
 *    2015-11-06 Mark Brasili/MRB:
 *                     - Per CG, use faster 500ms advertising rate instead of 10.24 seconds
 *    2015-11-17 Mark Brasili/MRB:
 *                     - Add second count in upper to bits of advertising flag field per Truefit request.  Their App will work better is data changes.
 *    2016-01-20 Mark Brasili/MRB:
 *                     - For better current draw, change code from GPIOTE back to button logic (1 mA -> 140 uA)
 *                     - Fix issue with button logic where pens had to be in a certain state when battery installed else could miss first pen removal.
 *                     - Add other current draw changes.  Still need to find more current draw issues (at 140 uA not 2-4 uA like SPS)
 *                     - Fix Pen removed duration logic (currently duration count not part of adv data)
 *    2016-01-21 Mark Brasili/MRB:
 *                     - Current Draw problems found in h/w design:
 *                       (1) If remove resister R2 current dropped from 140 uA to 3.3 uA (starts about 3.9 but slower goes down over minutes of running)
 *                       Per Jon Roney, the VCC_nRF connector to R2 on one side should really be a GPIO that can be turn on only
 *                       when reading temperature, or R2 resister can be left unpopulated (plus R3, C16 and NTC) and get temp from Nordic chip.
 *                       CG, advised for current boards remove R2, if we continue with a BLE Epipen (not Classis), we will add a 2nd gpio connection to the NTC (NTC=Negative Temperature Coefficient)
 *                       circuit;  The s/w will be changed to only drive this new GPIO when taking a termperature sample then to make it a input no-connect pin.
 *                       (2) switch circuit draws more current when switch closed (pen in holder).  (3.3 uA to 225 uA for Single Switch h/w, and 450 uA for Dual Switch h/w)
 *                       Per CG, if we contiue with a BLE Epipen the we will also change the h/w (maybe add an external pull down and s/w can remove the internal pull down on this gpio).
 *                     - Remove current draw test code and comments from code to clean it up.
 *
 * SMART EpiPen Phase I:
 *
 * 		2015-06-30 Mark Brasili/MRB: Receive email from CG with task assignment and following requirements.
 *                     - Use Pressure Switch s/w as base and modify s/w as follows
 *                     - Adv Mfg fields:
 *                       - Flag to indicate EPiPen in/out of holder - use button for initial testing
 *                       - Battery voltage in dV
 *                       - Chirp flag (buzzer is not currently chirping) - buzzer to be added use an LED for initial testing
 *                       - current temperature - initally use Nordic die temp, but better tollerance temp sensor to be added
 *                       - total time out of temperature range 68-77F (20-25C)
 *                       - Min temperature
 *                       - Max temperature
 *                     - Scanning optional, but need ability for App connected via BLE to:
 *                        - Active/deactivate chirp flag
 *                        - Clear temperature flags (total time, min/max temp)
 *                     Customer Mylan and Truefit is doing phone App s/w.
 *    2015-07-02 MRB:  Create 1st draft of "EpiPen Holder Advertising Data Packet Format" document
 *    2015-07-06 MRB:  Change Advertising data from Pressure Switch format to Epipen Format and add code to support new fields/flags.
 *    2015-07-06 MRB:  Deliver Adv format doc and prototype that advertises to Truefit.  Requirements changes to read temperature more often.
 *    2015-07-08 MRB:  Change how often read temperature from once per hour to once every 10 second tick.
 *    2015-07-08 MRB:  Add Scan Response capability (display UUID for new Flags Service with writable Flags characteristics
 *                     Can be enabled with compiler switch PROVIDE_SCAN_RESPONSE_DATA_TO_STACK, but not needed to connect
 *    2015-07-09 MRB:  Add Connectable logic, so phone App can connect to device and clear temperature flags and
 *                     activate/deactivate chirp flag (currently tied to the LED, but eventually will beep a buzzer for 30 seconds
 *                     to assist owner in finding his pen).  Used Nordic's github LED BUTTON Service (nrf51-ble-app-lbs-master)
 *										 example code as a reference.  Create customer UUID for ble_flags_svc using nRFGo Studio.
 *    2015-07-09 MRB:  Add faster advertising rate when pen out of holder (every 100 ms for 30 seconds) - CG, suggested change.
 *    2015-07-09 MRB:  Fix bugs found during testing [e.g., missing some calls to  ble_advdata_set() when change adv data, etc..
 *    2015-07-10 MRB:  Add non-requested feature: If write unknown command to flags characteristic - on next read of characteristic
 *                     can read the current state of the Adv Mfg flags field.
 *    2015-07-10 MRB:  Change advertising format "out of temperature" field from one byte to two now that counts 10 second ticks not hours.
 *
 *
 * SMART PRESSURE SWITCH Phase II:
 *    2015-06-08 Mark Brasili/MRB: Add Gary Craig's latest Phase I code to the Git Source Control Repository
 *    2015-05-08 MRB: Start Offical changes for Phase II, the new Advertising Format (hour/day/week counts, battery level,
 *               battery low flag, etc.).  Requirements in Gary's "Beascon Codes etc.xlsx doc.
 *    2015-05-12 MRB: Requirement added to provide temperature value in Advertising Mfg section too.
 *    2015-05-13 MRB: Charlie Green suggests additional requirement changes (e.g., reduced size Adv packet, no Scan rsp, more flag bits, etc.)
 *               See document titled "Powercast Smart Pressure Switch Advertising Data Packet Format (Vx).docx"
 *    2015-05-22 MRB: Check in Phase II code changes to be system tested with the Supera Filter and the pressure switch.
 *               Advertising Manufacturing Packet Changes:
 *               - Add previous hour, day and week event counters - see counter requirements in old spec "Beacon Codes etc.xlsx".
 *               - Add battery voltage (e.g., 0x1E = 30 = 3.0 volts)
 *               - Add signed temperature in Celcius  (e.g., 0x18 = 24 C = 75.2 F)
 *               - Calibrate die temperature reading as needed, per Nordic - found needed to subtract 10 degrees C from reading
 *                 for the initial prototype boards.
 *               - Add 3 digit serial number stored in flash at Mfg time
 *               - Add flag bits: temp too high (0x08), temp too low (0x04), battery low (0x02) & pressure okay (0x01)
 *               - Add logic to write temp too low and high flags to persistent flash and to restore them on reset (via UCIR registers)
 *               - Add logic to save power until unit installed and see normal pressure (go into System OFF mode)
 *               - Remove scan response packet.  Remove Partial list of services from Adv packet.
 *               - Remove code for unused services.
 *    2015-05-26 MRB: Requirement added to enable the Watchdog timeout to reset the unit if not kicked.  Total event count field in Adv
 *               Mfg data now a total count since last reset count.
 *               - Add logic to support a 25 Second Watchdog timeout.
 *    2015-06-02 MRB: New version of board available that uses bigger battery.  Flashed code for Dan to create initial Android phone App.
 *               Looks like temperature in C now too low (reading 0x0a or 10 C), maybe don't have to subtract 10 to calibrate anymore - TBD.
 *    2015-06-03 MRB: Requirement changes per CG:
 *               - Change Device type from 0x0100 to 0x5053 ("PS" for Pressure Switch)
 *               - Now that the battery can be inserted and removed, don't need to enter System Off mode until 1st event to save battery life.
 *                 May also not need to save temperature too high and low flags into flash either, but leave that logic in for now.
 *    2015-06-04 MRB: Make the following code changes:
 *               - Change Device type from 0x0100 to 0x5053 per new requirement above.
 *               - Use new Compiler Switch to disable going into System Off mode to save power per new requirement above.
 *               - Add new TEST_CODE compiler switch and support function to allow s/w engineer to write a custom serial number into flash.
 *                 This new process and logic may only be needed until Mfg process avaiable.  
 *                 See "CUSTOM SERIAL NUMBER PROCESS" below for process description.
 *               - Make temperature calibration value a #define and change from (-10) to (0), but need to test with more new boards. see note above.
 *               - Misc. clean-up (e.g., change variable name from "addr" to "serNum", etc..
 *
 * 
 * SMART PRESSURE SWITCH Phase I (Gary Craig/GC):
 * Note: I originally started working with the beacon code but decided that the Heart Rate Service demo was a better starting point.
 * Note: I will sometimes copy code from the beacon to this code and it will retain the old dates in the comments.
 * These notes are from the beacon code. I'll make corresponding changes to this hrs demo code.
 *		Beacon: 2014-10-27 Restarted with a fresh copy of the beacon code.
 *    Beacon: 2014-10-27 Changed advertising interval to 10 Seconds
 *		Beacon: 2014-10-28 Tried code from the GPIOTE demo but the current draw was way up.
 *		Beacon: 2014-10-29 Tried code from a file downloaded from a discussion on the developer form and
 *			it works but I have to intialize the IRQ after I start the beacon.
 *		Beacon: 2014-10-30 Cleaned up the code.
 *		Beacon: 2014-10-30 Added a 10 second counter and it had no real impact on current draw.
 *		Beacon: 2014-10-30 Change beacon time to 4 ms short of the 10.24s requirement. I think the beacon takes 4ms.
 *		Beacon: 2014-10-31 Tried to add a debounce routine which does NOT work good.
 *			I am going to put this aside to work on some of the things in the Firmware Requirements document.
 *			This is because I am unsure of the implications of all of these.
 *			I may have to change the timer structure completely especially since I have to initialize the IRQ after the beacon.
 *		Beacon: 2014-11-25 Started working on this code again after a long break. 
 * Change log
 * 		2014-11-25 Started working with this hrs demo code.
 *			Removed all references to their LEDs I have some which are really just debug pins.
 *		2014-11-25 Change beacon time to 4 ms short of the 10.24s requirement. I think the beacon takes 4ms.
 *		2014-11-26 Checked and found that all of the new includes I did in the Beacon code were already done in hrs.
 *		2014-11-26 Changed device name and manufacturer name per NetBlue. Note: short name here?
 *		2014-11-26 Shutting down simulations. For now the values will be fixed.
 *		2014-11-26 Shutting down heart rate service and changing appearance to a Generic Remote Control.
 *		2014-11-26 Removed ble_hrs.h include.
 *		2014-11-26 Shutting down RR Interval stuff.
 *		2014-11-26 Shutting down sensor contact stuff.
 *		2014-12-01 Starting to restructure Advertised Data.
 *			Removed ble_advdata.c and ble_advdata.h and replaced with customised copies called ble_advdata_NetBlue.c and .h
 *		2014-12-02 Succeeded in restructering the advertising data.
 *			Now the advertising data is like that in the firmware requirements other than it is not live data (the number of switch activations
 *				doesn't change). Also the order of the service data and the manufacturer specific data is swapped but I don't think that matters.
 *				It may take rewriting some of there code (which may not be possible) to get the order different.
 *			I tried to add a scan response packet but I don't know if it works because I am unsure of testing it.
 *		2014-12-03 Used Nordic's Master Control Panel on the PC and I could see that the correct data is in the Scan Response Packet.
 *			Added Model Number to Device Information Service.
 *		2014-12-03 Starting to make ADV Data dynamic. To do this I have to define the parameters in RAM rather than on the stack.
 *			This is so I can easily call ble_advdata_set() again without rebuildig the entire structure again.
 *		2014-12-03 Before making the ADV Data dynamic I discovered that I had NOT put the Tx level in the Scan Response Packet.
 *			Added the Tx level to the scan response packet.
 *		2014-12-10 Removing battery as a service.
 *			And shutting down the battery interupt.
 *		2014-12-10 Cleaning up the global data by moving some into advertising_init as static data.
 *			This will reduce clutter in the global namespace. For now this data will still be named m_* since I don't know what else to use.
 *		2014-12-11 Setting up monitoring of radio to detect scan requests form devices which are actively scanning me.
 *			We are now picking up scan requests and setting a bit in the 0xDEAD data if we believe that it was a scan request the previous time.
 *				THIS WAS DONE AWAY WITH AND THE WHOLE THING PUT ASIDE since I have been getting inconsistant readings. The values read differently
 *					before and after a connection is made. Raising priority of the interrupt did NOT help. I can't set it to no distance because
 *					the call to enable the interrupt fails with an error about the parameters.
 *					Note: I saw where it implied that using a distance > 1500 us would be more consistant but 1700us did NOT work.
 *		2014-12-11 Adding more limited discovery support but not enough to make it functional.
 *			Note: If you set the flags to Limited Discovery you have to also change the APP_ADV_TIMEOUT_IN_SECONDS to 30 seconds since this is
 *				the longest allowed time period for Limited Discovery.
 *		2014-12-12 I am moving on to add the 128bit uuid services.
 *		2014-12-16 Added a 8bit version of the Sleep Timer Service which does not do a sleep timer yet. For now it just lets you write a value.
 *			Note: The Service uses the long uuid but usually refers to it by the shorter STS_UUID_SERVICE of 0xB41E which is just masked out of the
 *				longer one like they do with the regular services which actually have a 128bit uuid associated with them. The characteristic below this
 *				however is just referenced by the short STS_UUID_ST_CHAR of 0xA378 and does not have the long uuid associated with it which according to
 *				the firmware requirements is a DIFFERENT long uuid than the service. I don't know if it is normal to have it different and I am not sure
 *				right now how to expose a different long one at this level but I will leave it like this for now.
 *		2014-12-16 Changed the Sleep Timer to 32bits.
 *		2014-12-17 Adding Pressure Event Log Service (PLS) with characteristics for Number Of Events and Pressure Event Log.
 *			It works but both NOE and PEL are just 32bit numbers. I made them read only.
 *			Note that the Sleep Timer is read/write but when it expires tht is not reflected. The read is just a verify of what you write.
 *		2014-12-18 Added 128bit uuids to the characteristics like Joey did.
 *			Matched uuids of the actual attributes to what Joey had.
 *		2015-02-25 Brought in routines to handle buttons from the LDS demo (nrf51-ble-app-lbs-master).
 *			These routines include bringing in app_gpiote.* app_button.* & app_scheduler.*
 *				They support waking up on button presses and button debouncing etc.
 *			These routines have not been hooked up to do the actual event counting yet.
 *			Tried hooking the routines up to count but it is not working.
 *		2015-02-26 Got debounce routine working. I was missing the final   app_button_enable()   call.
 *		2015-02-27 The unit is now counting any pressure events which are shorter than OUT_OF_WATER_TIME_PERIOD
 *			which is currently set for 40 seconds for test purposes. Note that time counting is still in 10 second intervals.
 *		2015-03-17 We want to change to a Nordic Device with less memory so I changed the Flash Configuration so it will use only the
 *			first 128K of the current device and it seems to work fine.
 *		2015-03-23 Started to make changes to make the unit NON-connectable.
 *
 *    <<< See Phase II History above >>>
 * 
 *
*/

// Most Include Files
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "nrf_gpio.h"
#include "nrf51_bitfields.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "ble_error_log.h"
#include "device_manager.h"
#include "ble_debug_assert_handler.h"
#include "pstorage.h"
#include "app_trace.h"
#include "app_gpiote.h"
#include "app_scheduler.h"
#include "nrf_temp.h"
#include "nrf_soc.h"
#include "app_util_platform.h"
#include "app_timer_appsh.h"  // for app_timer_event_t defintion - PORT change
#include "ble_gatts.h" // for ble_gatts_value_t - PORT change
#include "nrf_drv_config.h"  // for DUAL_SWITCH_EPIPEN_DEVICE compiler switch

#ifndef USE_BUTTON_APP_LIBRARY
#include "nrf_drv_gpiote.h"
#else
#include "app_button.h"
#endif

#define CONNECTABLE 1
#ifdef CONNECTABLE
#include "ble_conn_params.h"
//include "app_timer_appsh.h"
#include "ble_flags_svc.h"
//#include "bsp.h"
#include "ble_gap.h"
#endif

//#define USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9 1
//#define USE_EXAMPLE_CODE_PWM_SDK9 1

#ifdef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9
#include "nrf_gpio.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_gpiote.h"

// Pin for PWM/PPI to drive buzzer
#define GPIO_OUTPUT_PIN_NUMBER BUZZER_PIN

static nrf_drv_timer_t timer = NRF_DRV_TIMER_INSTANCE(2);  // timer0 not softdevice friendly, so use timer1  - also tried T2  - should this be a 1 not a 2?  >>> PWM if enabled uses 1

void timer_dummy_handler(nrf_timer_event_t event_type, void * p_context){}

#endif

#ifdef USE_EXAMPLE_CODE_PWM_SDK9
#include "app_pwm.h"
#endif
	
//#ifndef USE_BUTTON_APP_LIBRARY
static bool gbSwitch1Toggled = true;  // set true so initialized 1st time in main loop
#ifdef DUAL_SWITCH_EPIPEN_DEVICE	
static bool gbSwitch2Toggled = true;  // set true so initialized 1st time in main loop
//#endif	
#endif
static bool bCallChangeTxBeaconType = false;
	
static void advertising_init(bool reInit, bool forceIbeaconPenOut);

//#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
#define APPLE_COMPANY_ID 0x004C;	// Apple, Inc. Bluetooth Sig Company ID (see https://www.bluetooth.com/specifications/assigned-numbers/company-Identifiers)
//#else
// Company ID in Advertising packet = 0xFFFF
#define POWERCAST_COMPANY_ID 0x02D3;
//#endif


#ifdef DUAL_SWITCH_EPIPEN_DEVICE  // dual Epipen device
// Device Type In Adv Mfg Data section is now 0x4532 is ASCII "E2" for (E)pipen (2) slot:
// This should be more unique and help the phone App to filter out other device's Adv packets (along with Company ID, ...)
#define APP_DEVICE_TYPE_MSB		0x32
#define APP_DEVICE_TYPE_LSB		0x45	
#else	// single Epipen device:
// Device Type In Adv Mfg Data section is now 0x4550 is ASCII "EP" for (E)pi(P)en:
// This should be more unique and help the phone App to filter out other device's Adv packets (along with Company ID, ...)
#define APP_DEVICE_TYPE_MSB		0x50
#define APP_DEVICE_TYPE_LSB		0x45
#endif

#ifdef USE_16_BYTE_IBEACON_UUID_IN_PC_BEACON_MFG_DATA
#define APP_DEVICE_TYPE_MSB_SPECIAL		0x51
#endif

// This value can be set to zero for no calibration or a positive/negative number
// Original prototypes needed a value of negative ten (-10)
#define TEMP_IN_C_CALIBRATE_VALUE (0)

#define WATCHDOG_TIMEOUT_IN_SECONDS (25)

#ifdef CONVERT_BATTERY_VOLTAGE_TO_PERCENT
#include "app_util.h"  // for battery_level_in_percent() fcn def
#endif

#include "nrf_delay.h"

// Changed this when I made the ADV data go live.
#define IS_SRVC_CHANGED_CHARACT_PRESENT     0                           /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define BOND_DELETE_ALL_BUTTON_ID            BUTTON_1                   /**< Button used for deleting all bonded centrals during startup. */

#define DEVICE_NAME                          "Smart Epipen"      /**< Name of device. Will be included in the advertising data. */
#define LENGTH_OF_SHORTENED_NAME						6
#define MANUFACTURER_NAME                    "Powercast"                /**< Manufacturer. Will be passed to Device Information Service. Was NordicSemiconductor */
#define MODEL_NUMBER                    		 "SE-01"                    /**< Model Number. Will be passed to Device Information Service. */

#define FLAGS_FOR_LIM_DIS		BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE
#define FLAGS_FOR_GEN_DIS		BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE

#define APP_ADV_INTERVAL_LIM_DIS    MSEC_TO_UNITS(756, UNIT_0_625_MS) 	/**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */

// Powercast 2014-10-30 Changed advertising interval to 4ms short of the 10.24s requirement. I think that the beacon takes 4ms.
//#define APP_ADV_INTERVAL_GEN_DIS    MSEC_TO_UNITS(500, UNIT_0_625_MS) 	// 500 ms (1/2 second) (use to be 8)
#define NORMAL_ADV_RATE_SECONDS (1)
#define NORMAL_ADV_RATE_MS (NORMAL_ADV_RATE_SECONDS * 1000)
//#define IBEACON_ADV_RATE_SECONDS (5)
#define APP_ADV_INTERVAL_GEN_DIS    MSEC_TO_UNITS(NORMAL_ADV_RATE_MS, UNIT_0_625_MS) 	// 1000 ms (1 second) - Code changes adv format on second boundry, so could make this timer a little longer, to make sure don't send extra beacons when switch format
#define IBEACON_SECONDS_REPORT_PEN_OUT_OF_HOLD (30)  // reported by using ibeacon major byte 2 field = 1 for max of 30 seconds
#define IBEACON_MAX_REINIT_COUNT_FOR_PEN_OUT ((IBEACON_SECONDS_REPORT_PEN_OUT_OF_HOLD/NORMAL_ADV_RATE_SECONDS)/2)  // E.g. 30 sec/5 = 6/2 = 3 reinits - divide by 2 because reinits alternate PC beacons and iBeacons

// Old slower 10.236 second adv rate, left for future reference
//#define NORMAL_ADV_RATE_MS (10236)
//#define APP_ADV_INTERVAL_GEN_DIS    MSEC_TO_UNITS(NORMAL_ADV_RATE_MS, UNIT_0_625_MS) /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */

//#define APP_ADV_INTERVAL_100_MS    MSEC_TO_UNITS(100, UNIT_0_625_MS) /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */
#define APP_ADV_INTERVAL_100_MS    MSEC_TO_UNITS(100, UNIT_0_625_MS) // 100 ms - Code changes adv format on second boundy, so could make this timer a little longer (10x103=1030), to make sure don't send extra beacons when switch format

// Powercast 2014-11-25 Changed timeout from 180 to 0 to disable it GSC
#define APP_ADV_TIMEOUT_IN_SECONDS           0                                        	/**< The advertising timeout in units of seconds (before device quits completely). */

#define APP_TIMER_PRESCALER                  0                                          /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_MAX_TIMERS                 6                                          /**< Maximum number of simultaneously created timers. */
#define APP_TIMER_OP_QUEUE_SIZE              4                                          /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL                    MSEC_TO_UNITS(500, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.5 seconds). */
#define MAX_CONN_INTERVAL                    MSEC_TO_UNITS(1000, UNIT_1_25_MS)          /**< Maximum acceptable connection interval (1 second). */
#define SLAVE_LATENCY                        0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                     MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY       APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY        APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT         3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_TIMEOUT                    30                                         /**< Timeout for Pairing Request or Security Request (in seconds). */
#define SEC_PARAM_BOND                       1                                          /**< Perform bonding. */
#define SEC_PARAM_MITM                       0                                          /**< Man In The Middle protection not required. */
#define SEC_PARAM_IO_CAPABILITIES            BLE_GAP_IO_CAPS_NONE                       /**< No I/O capabilities. */
#define SEC_PARAM_OOB                        0                                          /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE               7                                          /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE               16                                         /**< Maximum encryption key size. */

#define DEAD_BEEF                            0xDEADBEEF                                 /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define EPIPEN_SWITCH_2_PIN 15  /* This will be P0.15 - dual epipen has two switches */
#define EPIPEN_SWITCH_1_PIN	14		/* This will be P0.14	*/
#define LED_PIN			9		  /* This will be P0.9 */
#define BUZZER_PIN 5              /* P0.05 */


// Powercast I got this from the LBS demo code.
#define APP_GPIOTE_MAX_USERS            1                                           /**< Maximum number of users of the GPIOTE handler. */
#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50, APP_TIMER_PRESCALER)    /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */
//#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(1, APP_TIMER_PRESCALER)    /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */
#define SCHED_MAX_EVENT_DATA_SIZE       sizeof(app_timer_event_t)                   /**< Maximum size of scheduler events. Note that scheduler BLE stack events do not contain any data, as the events are being pulled from the stack in the event handler. */
#define SCHED_QUEUE_SIZE                10                                          /**< Maximum number of events in the scheduler queue. */

#define ONE_SECOND_TIMER_INTERVAL				APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER) /** Time keeping interval (ticks). */
#define FIVE_SECOND_TIMER_INTERVAL		  APP_TIMER_TICKS( 5000, APP_TIMER_PRESCALER) /** Time keeping interval (ticks). */
#define TEN_SECOND_TIMER_INTERVAL				APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER) /** Time keeping interval (ticks). */
#define THIRTY_SECOND_TIMER_INTERVAL		APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER) /** Time keeping interval (ticks). */
#define ADC_SAMPLING_INTERVAL           APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER) /**< Sampling rate for the ADC */

#define BLE_UUID_PRESSURE_EVENT_SERVICE 0xDEAD     /**< Device Information service UUID. */

#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
// example ble app beacon uses 0xC3 not 0x03 - GELO uses 0xA8 - try 0xC3 because closer
#define APP_MEASURED_RSSI			0xC3		// Manufacturer specific information. The Beacon's measured TX power in this implementation
#else
#define APP_MEASURED_RSSI			0x03		// Manufacturer specific information. The Beacon's measured TX power in this implementation
#endif

#ifdef TEST_CODE_FOR_EVENT_COUNTERS
// mfg ADV prev counts for hour, day & week should be like: 01, 02, 02 (and last 02 increments in hex by 2, unless at MAX_BIT_COUNT test value
#define TICK_COUNT_IN_ONE_HOUR (1) 
#define TICK_COUNT_IN_ONE_DAY (2) 
#define HOURS_PER_DAY (2)
#define DAYS_PER_WEEK (7)
#define MAX_8_BIT_COUNT (0xFF)  // test with 0xFF & 0x0F
#else
#define TICK_COUNT_IN_ONE_HOUR ((60*60)/10) 	// 60 sec in a minute, 60 sec in an hour, timer 10 seconds
#define TICK_COUNT_IN_ONE_DAY ((60*60*24)/10) // 60 sec in a minute, 60 sec in an hour, 24 hours in a day, timer 10 seconds
#define HOURS_PER_DAY (24)
#define DAYS_PER_WEEK (7)
#define MAX_8_BIT_COUNT (0xFF)  // 0xFF = 255 decimal
#define MAX_16_BIT_COUNT (0xFFFF)  // 0xFFFF = 65,535 decimal
#endif

#define INDEX_LAST_HOUR (HOURS_PER_DAY - 1)
#define INDEX_LAST_DAY (DAYS_PER_WEEK - 1)

// High and Low thresholds per Charlie Greene
// Note: Nordic Chip's die Temperature sensor peripheral is +/- 4 C accurate, which is NOT included in high & low below
// Acceptable range is 68-77 F (or 20-25 C), so <= 19 C or >= 26 our of range:
#define TEMPERATURE_IN_C_HIGH_THRESHOLD (26)  // 26 C
#define TEMPERATURE_IN_C_LOW_THRESHOLD (19)	  // 19 C

#ifdef TEST_CODE_FOR_BATTERY_LOW_FLAG
#define BATTERY_MILLI_VOLT_LOW_THRESHOLD (3600)	// 3600 mV or 3.6 V (> 3 Volt battery so always low)
#else
#define BATTERY_MILLI_VOLT_LOW_THRESHOLD (2500)	// 2500 mV or 2.5 V - actual battery cutoff around 2 V
#endif

#ifdef TEST_CODE_USE_TEMPERATURE_THRESHOLD_COUNT_OF_ONE
#define TEMPERATURE_HIGH_COUNT_THRESHOLD (1) 	// High Count before set temp low flag
#define TEMPERATURE_LOW_COUNT_THRESHOLD (1)		// Low Count before set temp low flag
#else
#define TEMPERATURE_HIGH_COUNT_THRESHOLD (10) // High Count before set temp low flag
#define TEMPERATURE_LOW_COUNT_THRESHOLD (10)	// Low Count before set temp low flag
#endif


// Advertising Packets Manufacturing Sections Flag bit map definitions:
#define ADV_MFG_FLAG_2BITS_FOR_SECONDS_COUNT   (0xC0)
#define ADV_MFG_FLAG_BIT_FOR_EPIPEN_2_OUT_HOLDER (0x20)
#define ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_HIGH  (0x10)
#define ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_LOW   (0x08)
#define ADV_MFG_FLAG_BIT_FOR_BATTERY_LOW       (0x04)
#define ADV_MFG_FLAG_BIT_FOR_CHIRP_ACTIVE      (0x02)
//#define ADV_MFG_FLAG_BIT_FOR_SWITCH_STATE_OKAY (0x01)
#define ADV_MFG_FLAG_BIT_FOR_EPIPEN_OUT_HOLDER (0x01)
#define ADV_MFG_FLAG_BITS_USED_FOR_IBEACON_MAJOR_BYTE2_CHANGE (0x01)  // Use to be 0x1F (all but two high bits of second count), but now only use pen #1's out of holder flag bit 0 (0x01)

// Function Prototypes
static void advertising_start(void);

// Powercast variables
static app_timer_id_t                        m_1_second_timer_id;                        /**< 1 second timer but do work every 1, 4 and 10 seconds */
static app_timer_id_t mBackToNormAdvRateTimerId;

volatile static bool	ten_second_flag = false;
static uint32_t gCurEventTickDurationCount = 0;
static ble_advdata_t m_advdata;
static uint8_t m_flags = FLAGS_FOR_LIM_DIS;
static ble_advdata_t scanrsp;
		
#ifdef CONNECTABLE
//static ble_gap_sec_params_t             m_sec_params;                               /**< Security requirements for this application. */
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */
static ble_flags_t                      m_conn_flags;
#endif

#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA

 struct {
	uint8_t manufacturerData[2];	 
	uint8_t proximityUUIDBytes[16];
	uint8_t majorBytes[2];
	uint8_t minorBytes[2];
	//uint8_t minorByte1st;
	//uint8_t flags;  // alias minorByte2nd
	uint8_t rssiByte;
} m_adv_manuf_ibeacon_data;		// This will be the data itself in the manufacturer data (ibeacon format)

#endif  // PC EPIPEN format

// Manufacturer Specific Advertising Data:
 struct {
	uint8_t	app_device_type_LSB;
	uint8_t app_device_type_MSB;
	uint8_t serNum[3]; 
	uint8_t flags;
	 
#ifdef USE_16_BYTE_IBEACON_UUID_IN_PC_BEACON_MFG_DATA	
	uint8_t	proximityUUIDBytes[16];
#endif
	 
#ifdef USE_LONG_MFG_SPECIFIC_DATA		 // test to see if make mfg dta size smaller will get 128 bit UUIDs not 16 bit because adv data size < 31 bytes maximum
#ifdef ADV_MFG_DATA_CONTAINS_COUNT_OF_EVENTS	 
	uint8_t count_of_events[3];
#endif	 
	uint8_t batteryLevel;
  int8_t  temperatureInCel;
	uint16_t TotalTicksOutTempRange;
	int8_t  max_temp_in_Celcius;
	int8_t  min_temp_in_Celcius;
#endif	 // end USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
	//
} m_adv_manuf_data_data;		// This will be the data itself in the manufacturer data.


static ble_gap_adv_params_t		m_adv_params;                              /**< Parameters to be passed to the stack when starting advertising. */

volatile bool current_radio_active_state = false;
volatile bool current_radio_time_valid = true;

static dm_application_instance_t             m_app_handle;                              /**< Application identifier allocated by device manager */

static bool                                  m_memory_access_in_progress = false;       /**< Flag to keep track of ongoing operations on persistent memory. */

#if USE_STARDARD_ERR_HANDLER
/**@brief Function for error handling, which is called when an error has occurred.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of error.
 *
 * @param[in] error_code  Error code supplied to the handler.
 * @param[in] line_num    Line number where the handler is called.
 * @param[in] p_file_name Pointer to the file name.
 */
void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
	// Powercast Note: An Error is asserted
	
    // This call can be used for debug purposes during application development.
    // @note CAUTION: Activating this code will write the stack to flash on an error.
    //                This function should NOT be used in a final product.
    //                It is intended STRICTLY for development/debugging purposes.
    //                The flash write will happen EVEN if the radio is active, thus interrupting
    //                any communication.
    //                Use with care. Uncomment the line below to use.
    // ble_debug_assert_handler(error_code, line_num, p_file_name);

    // On assert, the system can only recover with a reset.
    //NVIC_SystemReset();
	  sd_nvic_SystemReset();  // softdevice friendly
}
#else
// Else use the Debugger Error Handler
#include "app_util_platform.h"

void app_error_handler(uint32_t error_code, uint32_t line_num, const uint8_t * p_file_name)
{
    // disable interrupts
    CRITICAL_REGION_ENTER();

    /* To be able to see function parameters in a debugger. */
    uint32_t temp = 1;
    while(temp);
    CRITICAL_REGION_EXIT();    
}
#endif


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


// Powercast functions

uint16_t gSwitchBeaconTypeSecCount = 0;

#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
void changeTxBeaconType(void)
{
		gSwitchBeaconTypeSecCount = 0;  // restart count, so softdevice sends beacon type for full time period
		sd_ble_gap_adv_stop();
		advertising_init(true, false);  // true = reinitializing (every other time swap between PC beacon and APPLE ibeacon), false = don't force ibeacon type
		advertising_start();	// restart advertising using new beacon type
}

void penJustPulled_RestartSendingIbeacons(void)
{
		gSwitchBeaconTypeSecCount = 0;  // restart count, so softdevice sends beacon type for full time period
		sd_ble_gap_adv_stop();
		advertising_init(true, true);  // true = reinitializing, true = force ibeacon type for full time period (with pen pulled) even if were sending ibeacons (pen not pulled)
		advertising_start();	// restart advertising using new beacon type
}
#endif


static void one_second_flag_timeout_handler(void * p_context)
{
		UNUSED_PARAMETER(p_context);
		static uint8_t tenSecCount = 0;
	
#if 1	
	  static uint8_t fourSecCount = 0;
	  uint8_t flags = m_adv_manuf_data_data.flags;
		uint32_t err_code;

	  fourSecCount++;  // wrap after four seconds count because 2 bits in flag field can hold 4 values (00, 01, 10, 11)
	  if (fourSecCount >=4)
    {
				fourSecCount = 0;
		}
	  flags &= ~ADV_MFG_FLAG_2BITS_FOR_SECONDS_COUNT;  // clear two bits in flags field
	  flags |= (fourSecCount & 0x03) << 6;  // set 2 bits in flags field equal to low order two bits of 4-seconds count
		m_adv_manuf_data_data.flags = flags;  // copy flags to adv data to be used by ble softdevice
				
#ifndef DONT_START_SOFTDEVICE_SO_CAN_DEBUG_EASIER					
		// Update Stack's ADV/SCan Data with change to flags field
		err_code = ble_advdata_set(&m_advdata, &scanrsp);
		// call error handling fcn if not NRF_SUCCESS
		APP_ERROR_CHECK(err_code);	
#endif	
#endif
		
	  tenSecCount++;  // count to 10, because main() loop reads temperature every 10 seconds
		
#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
	  gSwitchBeaconTypeSecCount++;
		
		// Don't switch to other advertisement format (ibeacon or normal), if connected and not advertising
		if ( m_conn_handle == BLE_CONN_HANDLE_INVALID)
		{
				int advRateSeconds = NORMAL_ADV_RATE_MS/1000;  // if 1000 ms, would be every second; if 5000 ms, would be every 5 seconds
				// Every normal advertising rate seconds, change adv format (so alternate between PC beacons and ibeacons)			
				if ((gSwitchBeaconTypeSecCount % advRateSeconds) == 0)
				{
						//changeTxBeaconType();
						bCallChangeTxBeaconType = true;
				}
		}
#endif		
		
		// wrap 10 second counter
		if (tenSecCount >= 10)
		{	
				ten_second_flag = true;  // set flag for main() loop so can read temperature every 10 seconds
				tenSecCount = 0;
		}
}


static void backToNormAdvRateTimeout_handler(void * p_context)
{
		UNUSED_PARAMETER(p_context);

		// Make sure using normal advertising rate after 30 second timeout
		if (m_adv_params.interval != APP_ADV_INTERVAL_GEN_DIS)
		{
				// Advertising stops when host connects, so may already be stopped when make next call
				sd_ble_gap_adv_stop();
			
				m_adv_params.interval = APP_ADV_INTERVAL_GEN_DIS;
			
				// If not connected, handle is invalid
				if ( m_conn_handle == BLE_CONN_HANDLE_INVALID)
				{
					  // Only start advertising again if not connected,
					  // else advertising_start() will be started again when find disconnected in on_ble_evt()
						advertising_start();
				}
		}
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    uint32_t err_code;

    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_MAX_TIMERS, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create Powercast timers.
		ten_second_flag = false;
		err_code = app_timer_create(&m_1_second_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                one_second_flag_timeout_handler);  // do work every 1, 4 and 10 seconds
    APP_ERROR_CHECK(err_code);

		err_code = app_timer_create(&mBackToNormAdvRateTimerId,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                backToNormAdvRateTimeout_handler);
	
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

		// Powercast 2014-11-26 Changing appearance
		err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_REMOTE_CONTROL);
    APP_ERROR_CHECK(err_code);
																					
    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Advertising functionality.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void advertising_init(bool reInit, bool forceIbeaconPenOut)
{
    uint32_t      err_code;


#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA	
		static int advInitCount = 0;
		static bool bCountingReinitsForPenOut = false;
		static uint8_t iBeaconReinitsForPenOut	 = 0;
#endif
	
#ifdef ONLY_BUMP_IBEACON_MAJOR_WHEN_FLAGS_CHANGE	
		static uint8_t savedFlags = 0;
#endif	

#ifdef USE_16_BYTE_IBEACON_UUID_IN_PC_BEACON_MFG_DATA
		static int pCAdvInitCount = 0;
#endif	

		// Being Static this data will really be in main memory rather than the stack but is here to prevent clutter in the global namespace.
		static ble_advdata_manuf_data_t m_adv_manuf_data;	// This will contain the manufacturer code and the manufacturer data itself.
		static ble_advdata_service_data_t	m_srv_data;
	
#ifdef CONNECTABLE
    //ble_advdata_t scanrsp;
		static ble_uuid_t m_adv_custom_uuids[] =
		{
			//{BLE_UUID_PRESSURE_EVENT_SERVICE,		BLE_UUID_TYPE_BLE},
			//{FLAGS_UUID_SERVICE, BLE_UUID_TYPE_BLE /*m_conn_flags.uuid_type*/},  // 16-bit UUID
			{FLAGS_UUID_SERVICE, BLE_UUID_TYPE_VENDOR_BEGIN /*m_conn_flags.uuid_type*/},  // 128-bit UUID
		};
#else
		static ble_uuid_t m_adv_custom_uuids[] =
		{
			{BLE_UUID_PRESSURE_EVENT_SERVICE,		BLE_UUID_TYPE_BLE},
		};		
#endif
	
		// Powercast generate manufacturer specific data.
		
		ble_gap_addr_t	app_bd_address_backwards; 	// Note: We WANT it backwards which is the way that it is returned from the funciton.		
		
#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
		// Initialize ibeacon packet even though may not sent ibeacon type yet
		m_adv_manuf_ibeacon_data.manufacturerData[0] = 0x02;  // beacon
		m_adv_manuf_ibeacon_data.manufacturerData[1] = 0x15;  // length of remaining 21 decimal bytes
		
		// Per Jeremy from Truefits's 4-20-16 email the UUID he generated is: 623C6997-13F0-4203-A9B8-02C68B59EA17		
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[0] = 0x62;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[1] = 0x3C;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[2] = 0x69;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[3] = 0x97;
		
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[4] = 0x13;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[5] = 0xF0;
		
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[6] = 0x42;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[7] = 0x03;
		
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[8] = 0xA9;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[9] = 0xB8;
		
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[10] = 0x02;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[11] = 0xC6;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[12] = 0x8B;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[13] = 0x59;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[14] = 0xEA;
		m_adv_manuf_ibeacon_data.proximityUUIDBytes[15] = 0x17;

#ifdef USE_BACKWARDS_GAP_ADDR_IN_LAST_6_OF_16_IBEACON_UUID_BYTES
		sd_ble_gap_address_get(&app_bd_address_backwards);
		memcpy(&m_adv_manuf_ibeacon_data.proximityUUIDBytes[10], app_bd_address_backwards.addr, sizeof(app_bd_address_backwards.addr));  // into offset 10-15
#endif
#ifdef USE_BACKWARDS_GAP_ADDR_IN_LAST_3_OF_16_IBEACON_UUID_BYTES
		sd_ble_gap_address_get(&app_bd_address_backwards);
		memcpy(&m_adv_manuf_ibeacon_data.proximityUUIDBytes[13], app_bd_address_backwards.addr, 3);  // into offsets 13-15
#endif

		// If starting up (e.g., not reinitializing)
		if (!reInit)
		{		
				// load default major and minor numbers
				m_adv_manuf_ibeacon_data.majorBytes[0] = 0x00;
				m_adv_manuf_ibeacon_data.majorBytes[1] = 0x02;  // was 1, but 1 now reserved for pen initially out of holder and used for 30 seconds, so start at 2
			
				m_adv_manuf_ibeacon_data.minorBytes[0] = 0x00;			
				m_adv_manuf_ibeacon_data.minorBytes[1] = 0x00;
				//m_adv_manuf_ibeacon_data.flags = 0;  // alias minorBytes2nd
		}
		
		m_adv_manuf_ibeacon_data.rssiByte = APP_MEASURED_RSSI;
		
#endif
		// PC EPIPEN format		

		m_adv_manuf_data_data.app_device_type_LSB = APP_DEVICE_TYPE_LSB;
		m_adv_manuf_data_data.app_device_type_MSB = APP_DEVICE_TYPE_MSB;
		
    sd_ble_gap_address_get(&app_bd_address_backwards);

		// Only use 3 of 6 bytes of Adv Address
		memcpy(m_adv_manuf_data_data.serNum, app_bd_address_backwards.addr, 3 /*sizeof(app_bd_address_backwards.addr)*/);  // Serial Number field per adv format doc

		// if starting up (not reinitializing) - clear flags and load starting max/min temperatures (app ignores min/max temp values)
		if (!reInit)
		{
				// Set Adv Mfg Flag fields
				m_adv_manuf_data_data.flags = 0;  // default to both pens in holder, incase 1st beacon gets out before poll these GPIOs before main loop

				#ifdef USE_LONG_MFG_SPECIFIC_DATA				
						m_adv_manuf_data_data.temperatureInCel = 127;  // set to max 8 bit value until do 1st reading
						// Initialize EpiPen's data:
						m_adv_manuf_data_data.TotalTicksOutTempRange = 0;  // clear on reset
						m_adv_manuf_data_data.max_temp_in_Celcius = -128;  // set to min 8 bit signed value until get 1st reading (so 1st reading higher and updates field)
						m_adv_manuf_data_data.min_temp_in_Celcius = 127;   // set to max 8 bit signed value until get 1st reading (so 1st reading lower and updates field)
				#endif
		}

#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
		int copyMajorByte2 = m_adv_manuf_ibeacon_data.majorBytes[1];		
		advInitCount++;
		if (((advInitCount % IBEACON_EVERY_xTH_BEACON) == 0) || (forceIbeaconPenOut))
		{
				// In case forcing ibeacon for Pen out - make sure count restarted, so on next timeout use PC beacon format
				//advInitCount &= 0xFE;  // make sure bit 0 cleared, so even
				advInitCount = 0;
			
				// On xth count, or if forcing, use Apple iBeacon adv format:
				m_adv_manuf_data.company_identifier = APPLE_COMPANY_ID;			
				m_adv_manuf_data.data.p_data = (uint8_t *) &m_adv_manuf_ibeacon_data;
				m_adv_manuf_data.data.size = sizeof(m_adv_manuf_ibeacon_data);
				//m_adv_manuf_ibeacon_data.flags = m_adv_manuf_data_data.flags;  // other code sets bits in right hand side, so copy to left for ibeacon

				// If startup, skip changing ibeacon majorBytes[1] just use default value 0x02 set above
				if (reInit)
				{
#ifdef ONLY_BUMP_IBEACON_MAJOR_WHEN_FLAGS_CHANGE
						// Only change the majorBytes[1] when there is a bit change in the bit-map flag field
						if (savedFlags != (m_adv_manuf_data_data.flags & ADV_MFG_FLAG_BITS_USED_FOR_IBEACON_MAJOR_BYTE2_CHANGE))
						{
								savedFlags = (m_adv_manuf_data_data.flags & ADV_MFG_FLAG_BITS_USED_FOR_IBEACON_MAJOR_BYTE2_CHANGE);							
#else
						{
#endif
							  // if ibeacon reporting pen out of holder for 1st 30 seconds it is out
								if (forceIbeaconPenOut)
								{
									m_adv_manuf_ibeacon_data.majorBytes[1] = 1;
									copyMajorByte2 = 1;
									bCountingReinitsForPenOut = false;  // set to false incase pen out and started counting, but before finished counting, pen inserted and pulled out again
								}
								else
								{
									copyMajorByte2 = m_adv_manuf_ibeacon_data.majorBytes[1];  // only read once because button_handler could interrupt this fnc
								}
								
								if (copyMajorByte2 == 1)  // fyi, set to one when button_handler() calls epipen_removed_from_holder()
								{
									// If this fnc just found out pen is out of holder,
									// leave at '1' (pen-out-of-holder) for 30 seconds (time calculated using a reinit count)
									if (!bCountingReinitsForPenOut)
									{
										bCountingReinitsForPenOut = true;
										iBeaconReinitsForPenOut = 1;  // start counting at 1
									}
									// else this fnc knew pen out, and is checking if 30 seconds over yet
									else
									{
										iBeaconReinitsForPenOut++;  // count ibeacon reinitializations done in this fnc
										// After so many ibeacon reinits, stop reporting pen out with major = 1
										// This is done so phone app doesn't keep reporting pen out again, because user may take days before puts a new pen in holder
										// Phone app logic can be codes so that for this time period to only report pen out once
										if (iBeaconReinitsForPenOut > IBEACON_MAX_REINIT_COUNT_FOR_PEN_OUT)
										{
											m_adv_manuf_ibeacon_data.majorBytes[1] = 2;  // restart major at beg of pen-in range 2-10 (even if pen still out)
											// clean-up flags to prepare for when a pen is re-inserted in holder and pulled again:
											iBeaconReinitsForPenOut = 0;
											bCountingReinitsForPenOut = false;
										}
									}
								}
								// else pen in holder, or reported out of holder for 30 seconds after last time pen out
								else
								{
									// Incrmeent the Major Byte to help app see iBeacons in background (e.g., looks like changing entry points when bumps)
									int newMajorByte2 = copyMajorByte2;
									newMajorByte2++;
									// Per https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/LocationAwarenessPG/RegionMonitoring/RegionMonitoring.html
									// Core Location limits to 20 the number of regions that may be simultaneously monitored by a single app. 
									// To work around this limit, consider registering only those regions in the user�s immediate vicinity.
									// As the user�s location changes, you can remove regions that are now farther way and add regions coming up on the user�s path.
									if (newMajorByte2 > 10)  // use to be 20, but now wrap at 10 because there can be two devices per user and phone app can only reg 20 total entry numbers
										newMajorByte2 = 2;  // was 1, but 1 now reserved for pen initially out of holder and used for 30 seconds, so start at 2
									m_adv_manuf_ibeacon_data.majorBytes[1] = newMajorByte2;
								}
						}
				}
#ifdef ONLY_BUMP_IBEACON_MAJOR_WHEN_FLAGS_CHANGE				
				else
				{
					savedFlags = m_adv_manuf_data_data.flags;
					// majorBytes[1] set near top of fnc
				}
#endif				
		}
		// Else time to send PC beacons
		else
		{
				m_adv_manuf_data.company_identifier = POWERCAST_COMPANY_ID;
				m_adv_manuf_data.data.p_data = (uint8_t *) &m_adv_manuf_data_data;			
						
#ifdef USE_16_BYTE_IBEACON_UUID_IN_PC_BEACON_MFG_DATA
				pCAdvInitCount++;
				// Every x th time, add the iBeacon proximity UUID bytes to the PC beacon
			  // Also below, need to also remove service UUID partial/complete list, so beacon not too long (e.g., use 16 byte iBeacon UUID instead of 16 byte Service UUID)
				if ((pCAdvInitCount % IBEACON_UUID_IN_PC_BEACON_EVERY_xTH_INIT_TIME) == 0)
				{
					  // copy 16 byte iBeacon UUID
						memcpy(m_adv_manuf_data_data.proximityUUIDBytes /*dest*/, m_adv_manuf_ibeacon_data.proximityUUIDBytes /*source*/, 16);
						m_adv_manuf_data.data.size = sizeof(m_adv_manuf_data_data);  // use full size
						m_adv_manuf_data_data.app_device_type_MSB = APP_DEVICE_TYPE_MSB_SPECIAL;  // change MSB of Device Type for this special PC beacon			
				}
				else
						m_adv_manuf_data.data.size = sizeof(m_adv_manuf_data_data) - 16;  // use full size minus size UUID not in beacon this time
#else			
				// Send normal ble format (with service UUID so iphone can connect)
				m_adv_manuf_data.data.size = sizeof(m_adv_manuf_data_data);
#endif
				
		}
#else  // else not using ibeacon stuff at all
    m_adv_manuf_data.company_identifier = POWERCAST_COMPANY_ID;		
    m_adv_manuf_data.data.p_data = (uint8_t *) &m_adv_manuf_data_data;
		m_adv_manuf_data.data.size = sizeof(m_adv_manuf_data_data);
#endif
		
		m_srv_data.service_uuid = BLE_UUID_PRESSURE_EVENT_SERVICE;
		
		m_flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    // ---------------------------------------------

    // Build and set advertising data.
    memset(&m_advdata, 0, sizeof(m_advdata));

		// Powercast changing ADV Data

    m_advdata.name_type               = BLE_ADVDATA_NO_NAME;
		m_advdata.include_appearance      = false;
    //m_advdata.flags.size              = sizeof(m_flags);  // PORT change
    //m_advdata.flags.p_data            = &m_flags;  // PORT change
		m_advdata.flags = m_flags;	// PORT change
    
    m_advdata.p_manuf_specific_data = &m_adv_manuf_data;

		m_advdata.p_service_data_array = &m_srv_data;
		
		m_advdata.service_data_count = 0;  // Remove Service data from end of Adv packet

		m_advdata.uuids_more_available.uuid_cnt = 0;  // Assume remove ServicesMoreAvailable (e.g., Partial List of Services - code 0x02) from Adv packet
		m_advdata.uuids_more_available.p_uuids  = m_adv_custom_uuids;
		m_advdata.uuids_complete.uuid_cnt = 0;  // Assume remove cOMPLETElISTuUID128 from Adv packet
		m_advdata.uuids_complete.p_uuids  = m_adv_custom_uuids;

#ifndef REMOVE_PARTIAL_LIST_SERVICES_FROM_ADV_PACKET
		m_advdata.uuids_more_available.uuid_cnt = sizeof(m_adv_custom_uuids) / sizeof(m_adv_custom_uuids[0]);  // don't remove
#endif

#ifndef REMOVE_COMPLETE_LIST_SERVICES_FROM_ADV_PACKET
		m_advdata.uuids_complete.uuid_cnt = sizeof(m_adv_custom_uuids) / sizeof(m_adv_custom_uuids[0]);  // don't remove
#endif

#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA
		// Check if time to use iBeacon format
		if (((advInitCount % IBEACON_EVERY_xTH_BEACON) == 0) || (forceIbeaconPenOut))
		{
				m_advdata.uuids_more_available.uuid_cnt = 0;  // Remove ServicesMoreAvailable (e.g., Partial List of Services - code 0x02) from Adv packet
				m_advdata.uuids_complete.uuid_cnt = 0;  // Remove cOMPLETElISTuUID128 from Adv packet
		}
		// else time for PC beacon adv format
		else
		{
				#ifdef USE_16_BYTE_IBEACON_UUID_IN_PC_BEACON_MFG_DATA
						// Need to remove service UUID partial/complete list, so PC beacon containing iBeacon UUID not too long (e.g., use 16 byte iBeacon UUID instead of 16 byte Service UUID)
						if ((pCAdvInitCount % IBEACON_UUID_IN_PC_BEACON_EVERY_xTH_INIT_TIME) == 0)
						{
								m_advdata.uuids_more_available.uuid_cnt = 0;  // Remove ServicesMoreAvailable (e.g., Partial List of Services - code 0x02) from Adv packet
								m_advdata.uuids_complete.uuid_cnt = 0;  // Remove cOMPLETElISTuUID128 from Adv packet			
						}
				#endif			
		}
#endif	
		
    memset(&scanrsp, 0, sizeof(scanrsp));
		
#ifdef PROVIDE_SCAN_RESPONSE_DATA_TO_STACK		
    scanrsp.uuids_complete.uuid_cnt = sizeof(m_adv_custom_uuids) / sizeof(m_adv_custom_uuids[0]);
    scanrsp.uuids_complete.p_uuids  = m_adv_custom_uuids;
#endif    
		// Update Stack's ADV/SCAN Data with any changes
    err_code = ble_advdata_set(&m_advdata, &scanrsp);  // NULL = no scan response data
    APP_ERROR_CHECK(err_code);

		if (!reInit)
		{		
				// Initialize advertising parameters (used when starting advertising).
				memset(&m_adv_params, 0, sizeof(m_adv_params));
		}

#ifdef CONNECTABLE
    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
#else
    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;  // Phase II non connectable (non-connectable for easier finding))
#endif		
    m_adv_params.p_peer_addr = NULL;                           // Undirected advertisement.
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
		
		if (!reInit)
		{
				// To help with connect times after chirp (and reset) use 100 MS for 30 seconds (timer started before main loop entered)
				m_adv_params.interval    = APP_ADV_INTERVAL_100_MS;  // faster time value at startup time (incase reset from chirp)
		}
		
    m_adv_params.timeout     = APP_ADV_TIMEOUT_IN_SECONDS;
}


#ifdef CONNECTABLE
static void flag_write_handler(ble_flags_t * p_flags, uint8_t flag_state)
{
	  uint16_t len = 1;
		ble_gatts_value_t gattsValue;  // PORT change
#ifdef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9	
	  static bool bFirstTime = true;
#endif	
	
    switch(flag_state)
    {
				case 0:  // DEACTIVATE CHIRP
						m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_CHIRP_ACTIVE);  // clear flag bit

#ifdef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9
						nrf_drv_timer_pause(&timer);  // Stop PWM buzzer event generation when App clears chirp flag
				
						// PC: Per nrF51822-PAN v3.0.pdf, do this workaround before start timer to prevent loosing one
      			// or more events from TIMER module (e.g., NRF_TIMERx->TASKS_STOP = 1; or NRF_TIMER_TASK_STOP)
						//*(uint32_t *)0x40008C0C = 0;  // PC : for Timer 0
						*(uint32_t *)0x40009C0C = 0;  // PC: for Timer 1
						//*(uint32_t *)0x4000AC0C = 0;  // PC: for Timer 2				
#endif
				
						break;
				
				case 1:  // ACTIVATE CHIRP
						m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_CHIRP_ACTIVE;  // set flag bit

#ifdef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9
						// PC: Per nrF51822-PAN v3.0.pdf, do this workaround before start timer to prevent loosing one
						// or more events from TIMER module (e.g., NRF_TIMERx->TASKS_START = 1; or NRF_TIMER_TASK_START)
						//*(uint32_t *)0x40008C0C = 1;  // PC added comment only: for Timer 0
						*(uint32_t *)0x40009C0C = 1;  // PC: for Timer 1 - NRF_TIMER1_BASE defined as 0x40009000UL, Noridic nrF51 Series Reference Manual doesn't define a register at offset 0xC0C?
						//*(uint32_t *)0x4000AC0C = 1;  // PC: for Timer 2
				
				    if (bFirstTime)
						{
								bFirstTime = false;
								nrf_drv_timer_enable(&timer);  // Start PWM buzzer event generation when App clears chirp flag
						}
						else
						{
								nrf_drv_timer_resume(&timer);  // Resume PWM buzzer event generation when App clears chirp flag
						}
#endif
						
						break;
				
				case 2:  // RESET TEMPERATURE FLAGs

#ifdef USE_LONG_MFG_SPECIFIC_DATA					
						m_adv_manuf_data_data.max_temp_in_Celcius = -128;  // set to min 8 bit signed value until get 1st reading (so 1st reading higher and updates field)
						m_adv_manuf_data_data.min_temp_in_Celcius = 127;   // set to max 8 bit signed value until get 1st reading (so 1st reading lower and updates field)
				    // Note: next temperature read will overwrite above values so user won't see above numbers in adv packets
						m_adv_manuf_data_data.TotalTicksOutTempRange = 0;
#endif				
						m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_HIGH);  // make sure bit cleared
						m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_LOW);   // make sure bit cleared
				
						ble_advdata_set(&m_advdata, &scanrsp);
						break;
				
				default:
						//Write Adv Mfg Flags field to flags attribute, so after phone writes to characteristic it can read the flags back
				    // PORT change
						//sd_ble_gatts_value_set(p_flags->flag_char_handles.value_handle /*attribute handle*/, 0 /*byte offset*/, &len /*byte length*/, &m_adv_manuf_data_data.flags /*pointer to buffer with desired value*/);
				    gattsValue.len = len;
				    gattsValue.offset = 0;
						gattsValue.p_value = &m_adv_manuf_data_data.flags;
						sd_ble_gatts_value_set(BLE_CONN_HANDLE_INVALID, p_flags->flag_char_handles.value_handle /*attribute handle*/, &gattsValue);
				    // end PORT change				
						break;					
    }
}


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t err_code;
    ble_flags_init_t init;
    
    init.flag_write_handler = flag_write_handler;
    
    err_code = ble_flags_init(&m_conn_flags, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in]   p_evt   Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}
#endif // #ifdef CONNECTABLE


/**@brief Function for starting application timers.
 */
static void application_timers_start(void)
{
    uint32_t err_code;

    // Start application timers.
    err_code = app_timer_start(m_1_second_timer_id, (ONE_SECOND_TIMER_INTERVAL), NULL);  // do work every 1, 4 and 10 seconds in one_second_flag_timeout_handler()
		APP_ERROR_CHECK(err_code);
	
	  // NOTE: Other App timers started when events received
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    uint32_t err_code;
    uint32_t count;

    // Verify if there is any flash access pending, if yes delay starting advertising until 
    // it's complete.
    err_code = pstorage_access_status_get(&count);
    APP_ERROR_CHECK(err_code);
    
    if (count != 0)
    {
        m_memory_access_in_progress = true;
        return;
    }
    
    err_code = sd_ble_gap_adv_start(&m_adv_params);
    APP_ERROR_CHECK(err_code);

    // Powercast Note: Advertising has started.
}


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
						// Powercast Note: End Advertising and Start Connected, but Phase II is non connectable

#ifdef CONNECTABLE
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
#endif
				
            break;

        case BLE_GAP_EVT_DISCONNECTED:

#ifdef CONNECTABLE
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
#endif
				
						advertising_start();						
				
            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISING)  // PORT - change was BLE_GAP_TIMEOUT_SRC_ADVERTISEMENT
							
            {
							// Powercast Note: End Advertising
              // Powercast Note: This is where limited advertisment will end and general will begin.
							
								#ifdef USE_SYSTEM_OFF_MODE_ON_GAP_EVT_TIMEOUT
										// Go to system-off mode (this function will not return; wakeup will cause a reset).
										NRF_POWER->SYSTEMOFF = 1;	// sd_power_system_off() not reliable						
								#else
										sd_nvic_SystemReset();  // softdevice friendly - reset and don't return
								#endif
                APP_ERROR_CHECK(err_code);

            }
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for handling the Application's system events.
 *
 * @param[in]   sys_evt   system event.
 */
static void on_sys_evt(uint32_t sys_evt)
{
    switch(sys_evt)
    {
        case NRF_EVT_FLASH_OPERATION_SUCCESS:
        case NRF_EVT_FLASH_OPERATION_ERROR:
            if (m_memory_access_in_progress)
            {
                m_memory_access_in_progress = false;
                advertising_start();
            }
            break;
        default:
            // No implementation needed.
            break;
    }
}

/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    on_ble_evt(p_ble_evt);

#ifdef CONNECTABLE	
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_flags_on_ble_evt(&m_conn_flags, p_ble_evt);
#endif

}


/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in]   sys_evt   System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    pstorage_sys_event_handler(sys_evt);
    on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;

    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(NRF_CLOCK_LFCLKSRC_XTAL_20_PPM, false);

#ifdef S110
    // Enable BLE stack 
    ble_enable_params_t ble_enable_params;
    memset(&ble_enable_params, 0, sizeof(ble_enable_params));

		// Powercast This may be the only item in the parameters GSC
    ble_enable_params.gatts_enable_params.service_changed = IS_SRVC_CHANGED_CHARACT_PRESENT;

		err_code = sd_ble_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
#endif

#ifdef CONNECTABLE
    ble_gap_addr_t addr;
    
    err_code = sd_ble_gap_address_get(&addr);
    APP_ERROR_CHECK(err_code);
    sd_ble_gap_address_set(BLE_GAP_ADDR_CYCLE_MODE_NONE, &addr);
    APP_ERROR_CHECK(err_code);
#endif

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for sys events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Device Manager events.
 *
 * @param[in]   p_evt   Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const    * p_handle,
                                           dm_event_t const     * p_event,
                                           ret_code_t           event_result)  // PORT change - was api_result_t																					 
{
    APP_ERROR_CHECK(event_result);
    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 */
static void device_manager_init(void)
{
    uint32_t                err_code;
    dm_init_param_t         init_data;
    dm_application_param_t  register_param;
    
    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    // Clear all bonded centrals if the Bonds Delete button is pushed.
    init_data.clear_persistent_data = (nrf_gpio_pin_read(BOND_DELETE_ALL_BUTTON_ID) == 0);

    err_code = dm_init(&init_data);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));
    
    //register_param.sec_param.timeout      = SEC_PARAM_TIMEOUT;  // PORT change	
    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for the GPIO initialization.
 *
 * @details Initializes all GPIO outputs used by this application. In the LDS demo this was called leds_init()
 */
static void output_pins_init(void)
{
		nrf_gpio_pin_clear(LED_PIN);
		nrf_gpio_cfg_output(LED_PIN);
	
		nrf_gpio_pin_clear(BUZZER_PIN);
		nrf_gpio_cfg_output(BUZZER_PIN);
}


/**@brief Function for the Event Scheduler initialization from the LBS demo.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}


/**@brief Function called when pen put in holder.
 *
 * @details Start Duration count of event which when event finished goes in Adv pck
 */
static void epipen_back_in_holder(uint8_t pin_no)
{
    uint32_t err_code;

		gCurEventTickDurationCount = 0;  // the pen re-inserted, make sure duration count cleared
#ifdef ADV_DATA_CONTAINS_DURATION_LAST_EVENT		
		// DO NOT CLEAR the Adv Mfg durationLastEvent field - it shows the current or previous pen pulled duration!
		//m_adv_manuf_data_data.durationLastEvent = gCurEventTickDurationCount;
#endif
	
#ifdef DUAL_SWITCH_EPIPEN_DEVICE								
		if (pin_no == EPIPEN_SWITCH_1_PIN)
		{
				m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_EPIPEN_OUT_HOLDER);  // make sure bit cleared (Epipen in holder)
			
#if USE_BUTTON_APP_LIBRARY	
				// workaround for if both pens in at reset time, bit for 2nd pin not getting reset
				nrf_delay_us(50*1000);
				if (nrf_gpio_pin_read(EPIPEN_SWITCH_1_PIN) == nrf_gpio_pin_read(EPIPEN_SWITCH_2_PIN))
				{
						m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_EPIPEN_2_OUT_HOLDER);											
				}
				// end workaround
#endif
						
		}
		else  // EPIPEN_SWITCH_2_PIN
		{
				m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_EPIPEN_2_OUT_HOLDER);  // make sure bit cleared (Epipen in holder)
												
#if USE_BUTTON_APP_LIBRARY		
				// workaround for if both pens in at reset time
				nrf_delay_us(50*1000);
				if (nrf_gpio_pin_read(EPIPEN_SWITCH_1_PIN) == nrf_gpio_pin_read(EPIPEN_SWITCH_2_PIN))
				{
						m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_EPIPEN_OUT_HOLDER);											
				}
				// end workaround
#endif
		}
#else
		m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_EPIPEN_OUT_HOLDER);  // make sure bit cleared (Epipen in holder)
#endif								
		
		// Update Stack's ADV/SCAN Data with any changes
		err_code = ble_advdata_set(&m_advdata, &scanrsp);
		APP_ERROR_CHECK(err_code);  // call error handling fcn if not NRF_SUCCESS
}



/**@brief Function called when pen removed.
 *
 * @details Increment event found in Advertising Mfg data
 */
static void epipen_removed_from_holder(uint8_t pin_no)
{
    uint32_t err_code;

		// Adv Mfg data may contains "Duration of last pen pull" field
		
		// Start duration tick count at 1 and bump it for each following
		// 10 second timer tick that follows until switch closes.
		// Duraction in seconds will equal count x 10.  Since set count
		// at 1 below, and don't know when next timer tick will fire, the
		// Adv duration may be up to 10 seconds shorter than reported.
		if(0 == gCurEventTickDurationCount)
		{
				gCurEventTickDurationCount = 1;  // start counting
#ifdef ADV_DATA_CONTAINS_DURATION_LAST_EVENT
				m_adv_manuf_data_data.durationLastEvent = gCurEventTickDurationCount;
#endif	
		}
		
		if (pin_no == EPIPEN_SWITCH_1_PIN)
		{		
				m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_EPIPEN_OUT_HOLDER;  // make sure bit set (Epipen out of holder)
			
#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA			
				//m_adv_manuf_ibeacon_data.majorBytes[1] = 1;  // 1 = a pen out of holder - change here (ASAP) so phone app sees on next ibeacon (which may be on its way out)
				penJustPulled_RestartSendingIbeacons();
#endif
			
#if USE_BUTTON_APP_LIBRARY
#ifdef DUAL_SWITCH_EPIPEN_DEVICE		
			  // workaround for if both pens out at reset time, bit for 2nd pin not getting set
				nrf_delay_us(50*1000);
				if (nrf_gpio_pin_read(EPIPEN_SWITCH_1_PIN) == nrf_gpio_pin_read(EPIPEN_SWITCH_2_PIN))
				{
						m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_EPIPEN_2_OUT_HOLDER;
				}
				// end workaround
#endif				
#endif
				
		}
#ifdef DUAL_SWITCH_EPIPEN_DEVICE		
		else  // EPIPEN_SWITCH_2_PIN
		{
				m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_EPIPEN_2_OUT_HOLDER;  // make sure bit set (Epipen out of holder)
			
#ifdef USE_IBEACON_FORMAT_MFG_SPECIFIC_DATA			
				//m_adv_manuf_ibeacon_data.majorBytes[1] = 1;  // 1 = a pen out of holder - change here (ASAP) so phone app sees on next ibeacon (which may be on its way out)
				penJustPulled_RestartSendingIbeacons();
#endif
			

#if USE_BUTTON_APP_LIBRARY
			  // workaround for if take both pens out at about same time, bit for 1st pin not getting set
				nrf_delay_us(50*1000);
				if (nrf_gpio_pin_read(EPIPEN_SWITCH_1_PIN) == nrf_gpio_pin_read(EPIPEN_SWITCH_2_PIN))
				{
						m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_EPIPEN_OUT_HOLDER;
				}
				// end workaround
#endif
		}					
#endif				

		// Update Stack's ADV/SCAN Data with any changes
		err_code = ble_advdata_set(&m_advdata, &scanrsp);  // NULL = no scan response data
		APP_ERROR_CHECK(err_code);  // call error handling fcn if not NRF_SUCCESS
		
		// Don't restart advertise at faster rate if connected
		if ( m_conn_handle == BLE_CONN_HANDLE_INVALID)
		{
				// Make sure using faster advertising interval when EpiPen out of holder
				if (m_adv_params.interval != APP_ADV_INTERVAL_100_MS)
				{
						sd_ble_gap_adv_stop();
						m_adv_params.interval = APP_ADV_INTERVAL_100_MS;
						advertising_start();
					
						// start 30 second timer - when timeout over, return to normal advertising rate
						err_code = app_timer_start(mBackToNormAdvRateTimerId, THIRTY_SECOND_TIMER_INTERVAL, NULL);
						APP_ERROR_CHECK(err_code);
				}
				else
				{
						// re-start 30 second timer (pen inserted and repulled before timeout) - when timeout over, return to normal advertising rate
						app_timer_stop(mBackToNormAdvRateTimerId);  // stop it then restart it
						err_code = app_timer_start(mBackToNormAdvRateTimerId, THIRTY_SECOND_TIMER_INTERVAL, NULL);
						APP_ERROR_CHECK(err_code);					
				}
		}
}

#ifdef USE_BUTTON_APP_LIBRARY
/**@brief Function for the handling button events taken from the LBS demo.
 *
 * @details Handles button events.
 */
static void button_event_handler(uint8_t pin_no, uint8_t button_action)
{

#ifdef CONNECTABLE
#ifdef CONNECTABLE_INCLUDES_BUTTON_NOTIFICATION_LOGIC
    uint32_t err_code;
#endif
#endif
	
    switch (pin_no)
    {
        case EPIPEN_SWITCH_1_PIN:
				case EPIPEN_SWITCH_2_PIN:
						
#if (1)				
						gbSwitch1Toggled = true;
#else				
						if(button_action) {
								// If didn't already call next fnc in polling code
								epipen_removed_from_holder(pin_no);	

						} else {
								epipen_back_in_holder(pin_no);
						}
#endif
						
#ifdef CONNECTABLE
#ifdef CONNECTABLE_INCLUDES_BUTTON_NOTIFICATION_LOGIC						
						err_code = ble_flags_on_button_change(&m_conn_flags, button_action);
            if (err_code != NRF_SUCCESS &&
                err_code != BLE_ERROR_INVALID_CONN_HANDLE &&
                err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
#endif						
#endif						
            break;

        default:
            APP_ERROR_HANDLER(pin_no);
            break;
    }
}
#else
void switch1_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
		gbSwitch1Toggled = true;
}

#ifdef DUAL_SWITCH_EPIPEN_DEVICE
void switch2_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
		gbSwitch2Toggled = true;
}
#endif
#endif

#ifdef USE_BUTTON_APP_LIBRARY

/**@brief Function for initializing the button handler module taken from the LBS demo.
 */
static void buttons_init(void)
{
    // Note: Array must be static because a pointer to it will be saved in the Button handler module
    static app_button_cfg_t buttons[] =
    {
#ifdef DUAL_SWITCH_EPIPEN_DEVICE				
        {EPIPEN_SWITCH_2_PIN, false, NRF_GPIO_PIN_PULLDOWN, button_event_handler},  // APP_BUTTON_ACTIVE_LOW (false)
#endif				
        {EPIPEN_SWITCH_1_PIN, false, NRF_GPIO_PIN_PULLDOWN, button_event_handler}  // APP_BUTTON_ACTIVE_LOW (false)
    };

    //APP_BUTTON_INIT(buttons, sizeof(buttons) / sizeof(buttons[0]), BUTTON_DETECTION_DELAY, true);  // PORT change
		uint32_t errCode = app_button_init(buttons, sizeof(buttons) / sizeof(buttons[0]), BUTTON_DETECTION_DELAY);
		APP_ERROR_CHECK(errCode);
		// end PORT change
}
#endif


/**@brief Function to initialize the ADC peripheral
 *
 * @details ADC used to measure battery voltage.
 */
static void adc_init(void)
{	
	// NOTE: the ADC is not a restricted peripheral per Softdevice specification,
	// so it is okay to access registers directly and there are no softdevice APIs for ADC.
	
	/* Enable interrupt on ADC sample ready event*/		
	NRF_ADC->INTENSET = ADC_INTENSET_END_Msk;   
	sd_nvic_SetPriority(ADC_IRQn, NRF_APP_PRIORITY_LOW);  
	sd_nvic_EnableIRQ(ADC_IRQn);

	// ADC CONFIG register's Reset values is what we need, so could remove the following register write:
	NRF_ADC->CONFIG	= (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) 	/* Bits 17..16 : ADC external reference pin selection. */
									| (ADC_CONFIG_PSEL_Disabled << ADC_CONFIG_PSEL_Pos)					/*!< Use analog input pins disabled */
									| (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)					/*!< Use internal 1.2V bandgap voltage as reference for conversion. */
									| (ADC_CONFIG_INPSEL_SupplyOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) /*!< VDD with 1/3 prescaling. */
									| (ADC_CONFIG_RES_8bit << ADC_CONFIG_RES_Pos);							/*!< 8bit ADC resolution. */ 
	
	/* Enable ADC*/
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;
}



/**@brief Function to start ADC sampling
 *
 * @details ADC sampling used to measure battery voltage.
 */
static void adc_sampling_start(void)
{
	uint32_t p_is_running = 0;
	
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled; // current draw change - enable/disable only when using peripheral (current stayed same, but keep)
		
	sd_clock_hfclk_request();
	while(! p_is_running) {  							//wait for the hfclk to be available
		sd_clock_hfclk_is_running((&p_is_running));
	}
	
	NRF_ADC->TASKS_START = 1;							//Start ADC sampling
	// ADC_IRQHandler() called when sample finished
}


/**@brief Interrupt handler for ADC data ready event
 *
 * @details The ADC peripheral measurement is used to measure the battery voltage.
 */
void ADC_IRQHandler(void)
{
	int32_t adc_result_read;

	// NOTE: the ADC is not a restricted peripheral per Softdevice specification,
	// so it is okay to access registers directly and there are no softdevice APIs for ADC.
	
	/* Clear dataready event */
  NRF_ADC->EVENTS_END = 0;	
	
	adc_result_read = NRF_ADC->RESULT;
	adc_result_read = (adc_result_read * 14.12);  // 14.12 mV per count (3000 = 3V) - 1.8V min chip needs, set low battery threshold to 184 per Jeff (2.6 V)
	// E.g., read on line powered dev kit board 0xFA = 250 * 14.12 = 3530 mV or 3.53V - stored in adv data as 0x0DCA

#ifdef USE_LONG_MFG_SPECIFIC_DATA
#ifdef CONVERT_BATTERY_VOLTAGE_TO_PERCENT
	m_adv_manuf_data_data.batteryLevel = battery_level_in_percent(adc_result_read);
#else	
	m_adv_manuf_data_data.batteryLevel = adc_result_read/100;  // 3000 / 100 = 30 = 3.0 V which will fit into 8 bits
#endif	
#endif
	
	//Use the STOP task to save current draw. Workaround for PAN_028 rev1.5 anomaly 1.
  NRF_ADC->TASKS_STOP = 1;
	
	//Release the external crystal
	sd_clock_hfclk_release();
	
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Disabled;  // current draw - disable peripheral (stayed same, but keep)
	
	// Set Adv Mfg packet Flag bit for battery level
	if (adc_result_read <= BATTERY_MILLI_VOLT_LOW_THRESHOLD)
	{
			m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_BATTERY_LOW;  // set bit at low threshold
	}
	else
	{
			m_adv_manuf_data_data.flags &= ~(ADV_MFG_FLAG_BIT_FOR_BATTERY_LOW);  // clear bit
	}
}	



/**@brief Function for getting die temperature from chip
 *
 * @details Get temperature data from chip, process and calibrate it and update the Advertising Packet structure
 *
 * @return unprocessed temperature reading from the API
 */
static int32_t temperature_data_get(void)
{
    int32_t temp;
	  int32_t retValue;
    uint32_t err_code;
	  static uint8_t countTempLow = 0;
	  static uint8_t countTempHigh = 0;
    
    err_code = sd_temp_get(&temp);
    APP_ERROR_CHECK(err_code);
	
	  retValue = temp;  // return 32 bit value not 8 bit value

#ifdef TEST_CODE_FOR_TEMP_AT_LOW_THRESHOLD
		retValue = temp = ((-20 + 10) * 4);  // temperature field in Adv packet should be 0xEC (-20 decimal)
#endif
	
#ifdef TEST_CODE_FOR_TEMP_AT_HIGH_THRESHOLD
		retValue = temp = ((54 + 10) * 4);  // temperature field in Adv packet should be 0x36 (54 decimal)
#endif

#ifdef TEST_CODE_FOR_TEMP_TOO_LOW
		retValue = temp = ((-129 + 10) * 4);  // temperature field in Adv packet should be 0x80 (-128 decimal - smallest 8 bit value)
#endif

#ifdef TEST_CODE_FOR_TEMP_TOO_HIGH
		retValue = temp = ((128 +10) * 4);  // temperature field in Adv packet should be 0x7F (-127 decimal - largest 8 bit value)
#endif

	  temp = (temp / 4);
	  temp = temp + (TEMP_IN_C_CALIBRATE_VALUE);  // calibrate because die temp may not be close to ambient temp, Calibrate/offset suggested by Nordic
	
		// Value from chip's TEMP register is 32 bits, so if too cold or too hot, tweak number:
		// Temperature range for an 8 bit value (packet field size) is: -128 to 127 Celsius, or -198.4 to 260.6 Fahrenheit
		// (don't expect values out of the above range, but if chip misbehaves don't want to make a large negitive # positive, or visa versa, so tweak #
		if (temp < -128)
		{
				temp = -128;  // use smallest negative 8 bit value that fits in 8 bit packet field
		}
		else if (temp > 127)
		{
				temp = 127;  // make largest positive 8 bit value that fits in 8 bit packet field
		}
		
#ifdef USE_LONG_MFG_SPECIFIC_DATA		
		// Put 8 bits of converted temp value into Adv Mfg packet:
		m_adv_manuf_data_data.temperatureInCel = (temp & 0x000000FF);
		
		if (temp < m_adv_manuf_data_data.min_temp_in_Celcius)
		{
				// save new min temp in adv pck mfg data
				m_adv_manuf_data_data.min_temp_in_Celcius = temp;   // new min
		}
	  // Don't make this an "else if" so max updates with above min the 1st time get temp reading
		if (temp > m_adv_manuf_data_data.max_temp_in_Celcius)
		{
				// save new min temp in adv pck mfg data
				m_adv_manuf_data_data.max_temp_in_Celcius = temp;   // new max
		}
#endif
		
		// Check if need to set Adv Mfg Flag field's temperature bits:
		if (temp <= TEMPERATURE_IN_C_LOW_THRESHOLD)
		{
			
#ifdef USE_LONG_MFG_SPECIFIC_DATA			
			  // Don't let 16 bit count wrap back to zero - leave it at max value of 0xFFFF
				if (m_adv_manuf_data_data.TotalTicksOutTempRange != 0xFFFF)
				{
						m_adv_manuf_data_data.TotalTicksOutTempRange++;
				}
#endif
				
#if 1			
			  // If flag not set already - continue
			  if ((m_adv_manuf_data_data.flags & ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_LOW) == 0)
				{
						if (countTempLow != MAX_8_BIT_COUNT)
						{
								// Don't let count wrap to zero
								countTempLow++;
						}
						// Don't set low flag until hit a count (e.g., ten different hours measurements saw temp too low)
						if (countTempLow >= TEMPERATURE_LOW_COUNT_THRESHOLD)
						{
							  // Save flag to persistent flash memory and read it back if device reset and update Adv Mfg Flag field
								//writeTempTooLowToFlash();

								// Make sure Adv Mfg Flag bit is set
								m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_LOW;
						}
				}
#endif
				
		}
		else if (temp >= TEMPERATURE_IN_C_HIGH_THRESHOLD)
		{
			
#ifdef USE_LONG_MFG_SPECIFIC_DATA			
			  // Don't let 16 bit count wrap back to zero - leave it at max value of 0xFFFF
				if (m_adv_manuf_data_data.TotalTicksOutTempRange != 0xFFFF)
				{
						m_adv_manuf_data_data.TotalTicksOutTempRange++;
				}
#endif
				
#if 1
			  // If flag not set already - continue
			  if ((m_adv_manuf_data_data.flags & ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_HIGH) == 0)
				{
						if (countTempHigh != MAX_8_BIT_COUNT)
						{
								// Don't let count wrap to zero
								countTempHigh++;
						}
						// Don't set high flag until hit a count (e.g., ten different hours measurements saw temp too low)
						if (countTempHigh >= TEMPERATURE_HIGH_COUNT_THRESHOLD)
						{
							  // Save flag to persistent flash memory and read it back if device reset and update Adv Mfg Flag field
								//writeTempTooHighToFlash();

								// Make sure Adv Mfg Flag bit is set
								m_adv_manuf_data_data.flags |= ADV_MFG_FLAG_BIT_FOR_TEMPERATURE_HIGH;
						}
				}
#endif
				
		}		
		
	  return (retValue);
}


/**@brief Function for doing period work.
 *
 * @details The code uses a ten second timerinterrupt that wakes up the device.
 *          statistics and BLE Mfg Data may be updated.
 */
static void doTenSecondWork(void)
{
		static uint16_t hoursTickCount = 0;
		static uint16_t daysTickCount = 0;
		static uint8_t hourIndex = 0;
		static uint8_t dayIndex = 0;
		static uint8_t hourEvents[HOURS_PER_DAY];
		static uint8_t dayEvents[DAYS_PER_WEEK];
		//int32_t temperature_data;	
		unsigned long temp = 0;
	
		hoursTickCount++;
	  daysTickCount++;
	
		if(0 != gCurEventTickDurationCount) {
				gCurEventTickDurationCount++;  // add another tick
#ifdef ADV_DATA_CONTAINS_DURATION_LAST_EVENT	
				m_adv_manuf_data_data.durationLastEvent = gCurEventTickDurationCount;
#endif
		}

		
#ifndef TEST_CODE_FORCE_WATCHDOG_TIMEOUT		
		// NOTE: The Watchdog is an Open (Non Restricted) Softdev peripheral
		NRF_WDT->RR[0] = WDT_RR_RR_Reload; //Reload watchdog register 0 every 10 second tick
#endif
		
		if (hoursTickCount >= TICK_COUNT_IN_ONE_HOUR)
		{
				// do Hour's work:
				hoursTickCount = 0;  // reset count, so detect next hour
				
				// Update day's event count each hour too:
			  temp = dayEvents[dayIndex] + hourEvents[hourIndex];
				if (temp > MAX_8_BIT_COUNT)
				{
						// If day's event count would wrap, limit count to max count that fits in 8 bits
					  dayEvents[dayIndex] = MAX_8_BIT_COUNT;  // 255 decimal or 0xFF
				}
				else
				{
					  // add day's event count to day's event count
						dayEvents[dayIndex] = temp;
				}
				
				// prepare for next hour:
				hourIndex++;
			  if (hourIndex > INDEX_LAST_HOUR)  // indexes 0-23 for 24 hours 
				{
						hourIndex = 0;  // wrap index after last hour of day
				}
				hourEvents[hourIndex] = 0;  // start next hour's count at zero (e.g., drop old data)
		}
	  else if (daysTickCount >= TICK_COUNT_IN_ONE_DAY)
		{
				// do Day's work:
				daysTickCount = 0;  // reset count, so detect next day
				
				// Prepare for next day
			  dayIndex++;
			  if (dayIndex > INDEX_LAST_DAY)  // indexes 0-6 for 7 days
				{
						dayIndex = 0;  // wrap index after loast day of week
				}
			  dayEvents[dayIndex] = 0;  // start next day's count at zero (e.g., drop old data)
				
#ifndef DONT_EVER_DO_BAT_N_TEMP_MEASUREMENTS
				// Start ADC sample - interrupt handler, ADC_IRQHandler, will be called when finished and it will update the ADV mfg data with the level
				adc_sampling_start();  // Once per day also read battery level
#endif				
		
		}
		
#ifndef DONT_EVER_DO_BAT_N_TEMP_MEASUREMENTS
		temperature_data_get();  // every 10 seconds read temperature
#endif		

}


/**@brief Function to enable the WatchDog timer.
 */
void wdt_init(void)
{
	// NOTE: The Watchdog is an Open (Non Restricted) Softdev peripheral
	NRF_WDT->CONFIG = (WDT_CONFIG_HALT_Pause << WDT_CONFIG_HALT_Pos) | ( WDT_CONFIG_SLEEP_Run << WDT_CONFIG_SLEEP_Pos);
	NRF_WDT->CRV = WATCHDOG_TIMEOUT_IN_SECONDS*32768;
	NRF_WDT->RREN |= WDT_RREN_RR0_Msk;  //Enable reload register 0
	NRF_WDT->TASKS_START = 1;
}


#ifndef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9
void driveBuzzer(void)
{
		int ii = 0;
		int jj = 0;
		int usDelay = 0;

	  // DISABLE INTERRUPTS TO PREVENT interrupting the buzzer:
		critical_region_enter();
	  // end of disable block
	
		// chip for 3 seconds then turn if off - app can restart
		for (jj = 0; jj < 3; jj++)
		{
				usDelay = 185;  // 2.7Khz / 2 (low for half and high for half)
				for (ii = 0; ii < 500000; ii += (usDelay * 2))  // loop for about 1/2 second at 2.7Khz
				{
					nrf_gpio_pin_clear(BUZZER_PIN);
					nrf_delay_us(usDelay);
											
					nrf_gpio_pin_set(BUZZER_PIN);
					nrf_delay_us(usDelay);
				}
				
				usDelay = 178;  // 2.8Khz / 2 (low for half and high for half)
				for (ii = 0; ii < 500000; ii += (usDelay * 2))  // loop for about 1/2 second at 2.8 Khz
				{
					nrf_gpio_pin_clear(BUZZER_PIN);
					nrf_delay_us(usDelay);
									
					nrf_gpio_pin_set(BUZZER_PIN);
					nrf_delay_us(usDelay);
				}
				
				nrf_gpio_pin_clear(BUZZER_PIN);

				usDelay = 250;  // 250 us
				for (ii = 0; ii < 500000; ii += usDelay)  // delay for 1/2 second with GPIO cleared
				{
					nrf_delay_us(usDelay);
				}

				usDelay = 250;  // 250 us
				for (ii = 0; ii < 500000; ii += usDelay)  // delay for 1/2 second with GPIO cleared
				{
					nrf_delay_us(usDelay);
				}
			}

			// Re-enable interrupts incase needed for sd_nvic_SystemReset() call
			critical_region_exit();
			
			// reset because sometimes after chirp 3 times and App reconnects, it can't disconnect
			sd_nvic_SystemReset();  // softdevice friendly
			NVIC_SystemReset();  // try by-passing softdevice
			// Should not return from either call above, but if do, let 11 second WDT reset board			
			for (ii = 0; ii < 1000; ii++)
			{
				  // 1000 * 26 ms = 26 seconds (WDT only 25 ms)
					nrf_delay_us((WATCHDOG_TIMEOUT_IN_SECONDS + 1) * 1000);  // delay 26 ms
			}
}
#endif


#ifdef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9
static void ppi_channel_setup()
{
    uint32_t compare_evt_addr;
    uint32_t gpiote_task_addr;
    nrf_ppi_channel_t ppi_channel;
    ret_code_t err_code;
    nrf_drv_gpiote_out_config_t config = GPIOTE_CONFIG_OUT_TASK_TOGGLE(false);

    err_code = nrf_drv_gpiote_out_init(GPIO_OUTPUT_PIN_NUMBER, &config);
    APP_ERROR_CHECK(err_code);
	
	  // NRF_TIMER_FREQ_1MHz values:
    //nrf_drv_timer_extended_compare(&timer, (nrf_timer_cc_channel_t)0, 200*1000UL, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);  // LED on for 200ms, off for 200ms, cycle time 400ms @ 1Mhz clk (1/1Mhz = 1 us)
	
	  // PC: NRF_TIMER_FREQ_31250Hz values (use with pc_default_config1):
    //nrf_drv_timer_extended_compare(&timer, (nrf_timer_cc_channel_t)0, 6UL, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);  // LED 185us on/off, cycle time 370us (2.7KHZ) @ 31250Hz (1/31250 = 32 us) 185/32=5.78125 round to 6
    //nrf_drv_timer_extended_compare(&timer, (nrf_timer_cc_channel_t)0, 5UL, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);  // LED 179us on/off, cycle time 370us (2.8KHZ) @ 31250Hz (1.32250 = 32 us) 179/32=5.5937 round down to 5

		
	  // PC: NRF_TIMER_FREQ_500kHz values (use with pc_default_config2):
    //nrf_drv_timer_extended_compare(&timer, (nrf_timer_cc_channel_t)0, 93UL, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);  // LED 185us on/off, cycle time 370us (2.7KHZ) @ 500KHz (1/500000 = 2 us) 185/2=92.5 round to 93
	
#if 0	  // timer channel 0
    nrf_drv_timer_extended_compare(&timer, (nrf_timer_cc_channel_t)0, 90UL, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);  // LED 179us on/off, cycle time 370us (2.8KHZ) @ 500KHz (1.500000 = 2 us) 179/2=89.5 round to 90

    err_code = nrf_drv_ppi_channel_alloc(&ppi_channel);
    APP_ERROR_CHECK(err_code);

    compare_evt_addr = nrf_drv_timer_event_address_get(&timer, NRF_TIMER_EVENT_COMPARE0);
#else  // timer channel 3
    nrf_drv_timer_extended_compare(&timer, (nrf_timer_cc_channel_t)3, 90UL, NRF_TIMER_SHORT_COMPARE3_CLEAR_MASK, false);  // LED 179us on/off, cycle time 370us (2.8KHZ) @ 500KHz (1.500000 = 2 us) 179/2=89.5 round to 90

    err_code = nrf_drv_ppi_channel_alloc(&ppi_channel);
    APP_ERROR_CHECK(err_code);

    compare_evt_addr = nrf_drv_timer_event_address_get(&timer, NRF_TIMER_EVENT_COMPARE3);
#endif

    gpiote_task_addr = nrf_drv_gpiote_out_task_addr_get(GPIO_OUTPUT_PIN_NUMBER);

    err_code = nrf_drv_ppi_channel_assign(ppi_channel, compare_evt_addr, gpiote_task_addr);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_ppi_channel_enable(ppi_channel);
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_out_task_enable(GPIO_OUTPUT_PIN_NUMBER);
}


// PC: Change default frequency from NRF_TIMER_FREQ_16MHz because too fast for time values over 4.096 ms
// T0 used in original SAADC example uses 32 bits (T0 is not softdevice friendly) , but T1 (softdevice friendly) uses only 16 bits
// T0 wraps after 2^32/16e6 = 268.44 seconds while T1/T2 wrap after 2^16/16e6 = 0.004096 seconds.
// For 1 second, need a freq that is < 100 times slower than default of 16 MHZ (1/0.004096 = 244.140625) - 16Mhz/250 = 64Khz
// NOTE: If Freq < 1 MHZ nrf52 uses PCLK1M instead of PCK16M for reduced power consumption, per NRF52 Manual's Timer Periperal section	
const nrf_drv_timer_config_t pc_default_config1 = { NRF_TIMER_FREQ_31250Hz, // slowest frequency choice (prescaler 9)
																									  NRF_TIMER_MODE_TIMER,   //TIMER1_CONFIG_MODE,
																									  NRF_TIMER_BIT_WIDTH_16, //TIMER1_CONFIG_BIT_WIDTH,
																									  NULL };

const nrf_drv_timer_config_t pc_default_config2 = { NRF_TIMER_FREQ_500kHz, // slowest frequency choice (prescaler 5)
																									  NRF_TIMER_MODE_TIMER,   //TIMER1_CONFIG_MODE,
																									  NRF_TIMER_BIT_WIDTH_16, //TIMER1_CONFIG_BIT_WIDTH,
																									  NULL };			

/**
 * @brief Function for application main entry.
 */
int buzzer_pwm_setup(void)
{
    ret_code_t err_code;
	
    nrf_delay_us(5000000);  // PC - gives 5 seconds to download new f/w (easier)

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);

    // May have already been called inside buttons_init() which is also called in main.c
    if (!nrf_drv_gpiote_is_init())
    {	
				err_code = nrf_drv_gpiote_init();
				APP_ERROR_CHECK(err_code);
		}
	
    //err_code = nrf_drv_timer_init(&timer, NULL, timer_dummy_handler);  // PC comment added only:  use default config - T0=NRF_TIMER_FREQ_1MHz (prescaler 4), T1=NRF_TIMER_FREQ_16MHz (prescaler 0) -T1 16 times faster
    //err_code = nrf_drv_timer_init(&timer, &pc_default_config1, timer_dummy_handler);  // PC:    Use Powercast/PC T1 config (e.g., NRF_TIMER_FREQ_31250Hz - prescaler 9)
    err_code = nrf_drv_timer_init(&timer, &pc_default_config2, timer_dummy_handler);  // PC:    Use Powercast/PC T1 config (e.g., NRF_TIMER_FREQ_500kHz - prescaler 5)
 	  APP_ERROR_CHECK(err_code);
															
		// PC: per nRF51822-PAN v3.0.pdf, need following work arounds	to prevent loosing one or more events from TIMER module
	  // PC: Do this workaround before start timer (e.g., NRF_TIMERx->TASKS_START = 1;)
		//*(uint32_t *)0x40008C0C = 1;  // PC added comment only: for Timer 0
		//*(uint32_t *)0x40009C0C = 1;  // PC: for Timer 1 - NRF_TIMER1_BASE defined as 0x40009000UL, Noridic nrF51 Series Reference Manual doesn't define a register at offset 0xC0C?
		//*(uint32_t *)0x4000AC0C = 1;  // PC: for Timer 2
	
	  // PC: Do this workaround after top timer (e.g., NRF_TIMERx->TASKS_STOP = 1;)
		//*(uint32_t *)0x40008C0C = 0;  // PC : for Timer 0
		//*(uint32_t *)0x40009C0C = 0;  // PC: for Timer 1
		//*(uint32_t *)0x4000AC0C = 0;  // PC: for Timer 2		
	
    // Setup PPI channel with event from TIMER compare and task GPIOTE pin toggle.
    ppi_channel_setup();

#if 0 // test code >>> this code block sounds the buzzer via PPI, but if don't start it and return and setup ble, ble doesn't advertise, but it is running the main loop
    // Enable timer
		
		// PC: Per nrF51822-PAN v3.0.pdf, do this workaround before start timer to prevent loosing one
		// or more events from TIMER module (e.g., NRF_TIMERx->TASKS_START = 1; or NRF_TIMER_TASK_START)
		//*(uint32_t *)0x40008C0C = 1;  // PC added comment only: for Timer 0
		*(uint32_t *)0x40009C0C = 1;  // PC: for Timer 1 - NRF_TIMER1_BASE defined as 0x40009000UL, Noridic nrF51 Series Reference Manual doesn't define a register at offset 0xC0C?
		*(uint32_t *)0x4000AC0C = 1;  // PC: for Timer 2
    nrf_drv_timer_enable(&timer);  // Call this when set chirping flag the 1st time
		
#endif		
		return(1);
}
	
#endif


#ifdef USE_EXAMPLE_CODE_PWM_SDK9
APP_PWM_INSTANCE(PWM1,1);                   // Create the instance "PWM1" using TIMER1.   >>> MRB: If run this line, with call to buzzer_pwm_setup() commented out, ble adv doesn't work???  If comment out compiler switch is advertises
//APP_PWM_INSTANCE(PWM1,2);                   // Create the instance "PWM1" using TIMER2.

static volatile bool ready_flag;            // A flag indicating PWM status.

void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    ready_flag = true;
}

int buzzer_pwm_setup(void)
{
    ret_code_t err_code;
	
    nrf_delay_us(5000000);  // PC - gives 5 seconds to download new f/w (easier)	
    
    /* 2-channel PWM, 200Hz, output on DK LED pins. */
    //app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, BSP_LED_0, BSP_LED_1);
		app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_2CH(5000L, LED_PIN, BUZZER_PIN);
    
    /* Switch the polarity of the second channel. */
    pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
    
    /* Initialize and enable PWM. */
    err_code = app_pwm_init(&PWM1,&pwm1_cfg,pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
    app_pwm_enable(&PWM1);
    
    // next code block  copied to bottom of main loop, and this block commented out, doesn't work, get hardfault when jump to main() from  arm_startup_nrf51.s lin
    uint32_t value;
    while(true)
    {
        for (uint8_t i = 0; i < 40; ++i)
        {
            value = (i < 20) ? (i * 5) : (100 - (i - 20) * 5);
            
            ready_flag = false;
            /* Set the duty cycle - keep trying until PWM is ready... */
            while (app_pwm_channel_duty_set(&PWM1, 0, value) == NRF_ERROR_BUSY);
            
            /* ... or wait for callback. */
            while(!ready_flag);
            APP_ERROR_CHECK(app_pwm_channel_duty_set(&PWM1, 1, value));

					  nrf_delay_us(25*1000);
        }
    }
    return(1);
}

#endif


/**@brief Function for application main entry.
 *
 * @details 
 */
int main(void)
{
    uint32_t	err_code;
	
		//sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);  // for reduced current draw (didn't help)

		//sd_power_mode_set(NRF_POWER_MODE_LOWPWR);	 // for reduced current draw (didn't help)

	  // Start initialization
		output_pins_init();  			// setup switch/button external gpio pin pull up
	
    timers_init();  		// create 10 second timer

#ifdef USE_BUTTON_APP_LIBRARY
  	buttons_init();			// define button_event_handler for device (acts like a button) init - PORT change - app_button_init() called inside buttons_init() calls nrf_drv_gpiote_in_init(), so remove call to gpiote_init_partA()
#endif	

#ifdef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9	
	  buzzer_pwm_setup();
#endif

#ifdef USE_EXAMPLE_CODE_PWM_SDK9
	  buzzer_pwm_setup();
#endif


#ifndef DONT_START_SOFTDEVICE_SO_CAN_DEBUG_EASIER
		ble_stack_init();		// BLE stack and softdevice init
#endif

#define USE_SDK6_SCHEDULER 1
#ifdef USE_SDK6_SCHEDULER	
    scheduler_init();		// event scheduler init
#endif		
		
#ifndef DONT_START_SOFTDEVICE_SO_CAN_DEBUG_EASIER		
		device_manager_init();  // enable APp to manage the softdevice on a top level
    gap_params_init();
		
#ifdef CONNECTABLE
		services_init();
#endif

    advertising_init(false, false);  // 1st false = not reinitializing, this is 1st init - 2nd false = don't for iBeacon for pen out

#ifdef CONNECTABLE
    conn_params_init();
#endif
#endif

#ifndef DONT_EVER_DO_BAT_N_TEMP_MEASUREMENTS
		adc_init();          //ADC used for battery level measurement
#endif

    application_timers_start();  // start 10 second timer

#ifdef USE_BUTTON_APP_LIBRARY
    err_code = app_button_enable();
    APP_ERROR_CHECK(err_code);
#endif

#ifdef TEST_CODE_PUT_TICK_COUNT_IN_MFG_DEV_TYPE_LSB
		m_adv_manuf_data_data.app_device_type_LSB = 0;  // field used to count # ten second ticks for debug
#endif		
		
#ifndef DONT_EVER_DO_BAT_N_TEMP_MEASUREMENTS
		// get initial temperature and battery level and store in Adv Mfg packet
		temperature_data_get();

		// get initial ADC sample - interrupt handler, ADC_IRQHandler, will be called when finished and it will update the ADV mfg data with the level
		adc_sampling_start();
		// NOTE: debugger shows that get adc interrupt before execute next line and before call advertising_start() below, so battery flag valid on 1st beacon
#endif

#ifndef DONT_START_SOFTDEVICE_SO_CAN_DEBUG_EASIER
		// Update Stack's ADV/SCAN Data with any changes
		err_code = ble_advdata_set(&m_advdata, &scanrsp);  // NULL = no scan response data
		APP_ERROR_CHECK(err_code);  // call error handling fcn if not NRF_SUCCESS

		// Do after call temperature_data_get() above:
	  // start blue tooth advertising (e.g., every 10 seconds send an advertising packet via radio)
		advertising_start();
#endif

		//nrf_gpio_pin_set(LED_PIN);  // can be used for testing

		wdt_init();  // initialize the WatchDog timer (Open/Non Restricted Softdev peripheral)

#if USE_BUTTON_APP_LIBRARY
// If pen out of holder at startup time, set flag bits in adv data
#ifdef DUAL_SWITCH_EPIPEN_DEVICE		
		bool pin2 = nrf_gpio_pin_read(EPIPEN_SWITCH_2_PIN);  // 0 = Epipen out (switch 2 up), 1 = Epipen in (switch 2 pushed down)
																													 // switch 2 is one next to 4 holes in board for debug probe  connection
#endif
		bool pin1 = nrf_gpio_pin_read(EPIPEN_SWITCH_1_PIN);  // 0 = Epipen out (switch up), 1 = Epipen in (switch pushed down)		
		if (!pin1)
		{
				epipen_removed_from_holder(EPIPEN_SWITCH_1_PIN);
		}
#ifdef DUAL_SWITCH_EPIPEN_DEVICE		
		else if (!pin2)
		{
		  epipen_removed_from_holder(EPIPEN_SWITCH_2_PIN);
		}
#endif
#endif

#ifndef USE_EXAMPLE_CODE_PWM_SDK9
#ifndef USE_BUTTON_APP_LIBRARY		
    if (!nrf_drv_gpiote_is_init())
    {	
				err_code = nrf_drv_gpiote_init();
				APP_ERROR_CHECK(err_code);
    }

    // Initialize GPIOTE switch pins so get events when gpios toggle
    nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    //in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;		
    err_code = nrf_drv_gpiote_in_init(EPIPEN_SWITCH_1_PIN, &in_config, switch1_event_handler);
    APP_ERROR_CHECK(err_code);
		nrf_drv_gpiote_in_event_enable(EPIPEN_SWITCH_1_PIN, true /*interrupt enable*/);
#ifdef DUAL_SWITCH_EPIPEN_DEVICE		
    nrf_drv_gpiote_in_config_t in_config2 = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
    //in_config2.pull = NRF_GPIO_PIN_NOPULL;
    in_config2.pull = NRF_GPIO_PIN_PULLDOWN;		
    err_code = nrf_drv_gpiote_in_init(EPIPEN_SWITCH_2_PIN, &in_config2, switch2_event_handler);
    APP_ERROR_CHECK(err_code);
		nrf_drv_gpiote_in_event_enable(EPIPEN_SWITCH_2_PIN, true /*interrupt enable*/);	
#endif
#endif
#endif

		
		// start 30 second timer to change from APP_ADV_INTERVAL_100_MS to APP_ADV_INTERVAL_GEN_DIS
		// 100 ms helps with connect times for multiple chirps (reset after each chirp)
		err_code = app_timer_start(mBackToNormAdvRateTimerId, THIRTY_SECOND_TIMER_INTERVAL, NULL);
		APP_ERROR_CHECK(err_code);
		
		// Enter main loop.
    for (;;)
    {
				//nrf_gpio_pin_set(LED_PIN);  // test code
				//nrf_delay_us(150*1000);
				//nrf_gpio_pin_clear(LED_PIN);
			
#ifndef USE_EXAMPLE_CODE_GPIOTE_PPI_SDK9			

//#define LET_APP_DISCONNECT_NOT_DEVICE 1			
#ifdef LET_APP_DISCONNECT_NOT_DEVICE  // original logic
				// If App not connected (handle is invalid) - check chirp flag
				if ( m_conn_handle == BLE_CONN_HANDLE_INVALID)
				{
					  // Start chirping after App disconnects
						if (m_adv_manuf_data_data.flags & ADV_MFG_FLAG_BIT_FOR_CHIRP_ACTIVE)
						{
								driveBuzzer();
						}
				}
#else	// epipen device disconnects for better performance with their App
				// let device (Epipen) disconnect, not App, so chirp happens faster (Truefit iphone app takes few seconds to disconnect)
				
				// Start chirping after disconnected
				if ( m_adv_manuf_data_data.flags & ADV_MFG_FLAG_BIT_FOR_CHIRP_ACTIVE)
				{
					  // If App and device not connected (handle is invalid) - check chirp flag
						if (m_conn_handle == BLE_CONN_HANDLE_INVALID)
						{
								driveBuzzer();
						}
						else
						{
								err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
								//APP_ERROR_CHECK(err_code);  // don't check err code - it could be an err because disconnection already in progress
							
								// BLE_GAP_EVT_DISCONNECTED will indicate disconnect completed and code will come back in this block and call driveBuzzer()
						}
				}
			
#endif
#endif

//#ifndef USE_BUTTON_APP_LIBRARY
				if (gbSwitch1Toggled)
				{
						app_timer_stop(m_1_second_timer_id);  // restart 1 second timer, so don't timeout right after setting up ibeacon format and timeout logic changes format back to normal beacon before softdevice had a chance to send the ibeacon
						gbSwitch1Toggled = false;  // clear before do work in case get another event
					
						bCallChangeTxBeaconType = false;  // if timeout happened about same time, forget about timeout, so make sure send ibeacon format
						uint32_t pinState = nrf_gpio_pin_read(EPIPEN_SWITCH_1_PIN);
						if (pinState != 0)
						{
								epipen_back_in_holder(EPIPEN_SWITCH_1_PIN);
						}
						else
						{
								epipen_removed_from_holder(EPIPEN_SWITCH_1_PIN);
						}
						// Start application timers.
						err_code = app_timer_start(m_1_second_timer_id, (ONE_SECOND_TIMER_INTERVAL), NULL);  // do work every 1, 4 and 10 seconds in one_second_flag_timeout_handler()
						APP_ERROR_CHECK(err_code);						
				}
				else if (bCallChangeTxBeaconType) {  // check if timed out
						bCallChangeTxBeaconType = false;
						changeTxBeaconType();
				}
				
//#ifdef DUAL_SWITCH_EPIPEN_DEVICE				
//				if (gbSwitch2Toggled)
//				{
//						gbSwitch2Toggled = false;  // clear before do work in case get another event
//						uint32_t pinState = nrf_gpio_pin_read(EPIPEN_SWITCH_2_PIN);
//						if (pinState != 0)
//						{
//								epipen_back_in_holder(EPIPEN_SWITCH_2_PIN);
//						}
//						else
//						{
//								epipen_removed_from_holder(EPIPEN_SWITCH_2_PIN);
//						}
//				}				
//#endif				
//#endif
				
			  // if 10 second timer interrupt fired:
				if(true == ten_second_flag)
				{
					ten_second_flag = false;

#ifdef TEST_CODE_PUT_TICK_COUNT_IN_MFG_DEV_TYPE_LSB					
					m_adv_manuf_data_data.app_device_type_LSB++;  // count 10 second ticks for debug
#endif					
					
					doTenSecondWork();  // do 10 second  plus hour and day work if appropriate

#ifndef DONT_START_SOFTDEVICE_SO_CAN_DEBUG_EASIER					
					// Update Stack's ADV/SCan Data with any changes in the past 10 Seconds.
					err_code = ble_advdata_set(&m_advdata, &scanrsp);
					// call error handling fcn if not NRF_SUCCESS
					APP_ERROR_CHECK(err_code);
#endif					
				}

#ifdef USE_SDK6_SCHEDULER	
				// process all events since last time called:
				app_sched_execute();
#endif				

//#ifdef USE_EXAMPLE_CODE_PWM_SDK9
//				int value;
//        for (uint8_t i = 0; i < 40; ++i)
//        {
//            value = (i < 20) ? (i * 5) : (100 - (i - 20) * 5);
//            
//            ready_flag = false;
//            /* Set the duty cycle - keep trying until PWM is ready... */
//            while (app_pwm_channel_duty_set(&PWM1, 0, value) == NRF_ERROR_BUSY);
//            
//            /* ... or wait for callback. */
//            while(!ready_flag);
//            APP_ERROR_CHECK(app_pwm_channel_duty_set(&PWM1, 1, value));

//					  nrf_delay_us(25*1000);
//        }
//#endif		    
				// Go to sleep until interrupt/event:
        power_manage();
    }
}


