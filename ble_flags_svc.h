/*
 * Copyright (c) 2015 Powercast.  All Rights Reserved.
 * This is a connectable service that can be used to set/clear application flags.
 * Modified the Nordic nrf51-ble-app-lbs (LED BUTTON SRV) example code.
 */

/* Copyright (c) 2013 Nordic Semiconductor. All Rights Reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the license.txt file.
 */

#ifndef BLE_FLAGS_H__
#define BLE_FLAGS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

// Uncomment to use, but not needed for Epipen and not tested (was in lbs example code)
//#define CONNECTABLE_INCLUDES_BUTTON_NOTIFICATION_LOGIC 1

// LBS example code's custom UUIDs - left for reference only
//#define FLAGS_UUID_BASE {0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}
//#define FLAGS_UUID_SERVICE 0x1523
//#define FLAGS_UUID_FLAG_CHAR 0x1525
//#define FLAGS_UUID_BUTTON_CHAR 0x1524


// New EpiPen custom UUID created with nRfGo Studio's nRF8001 Setup menu "Edit 128-bit-UUID" options, clicked "Add new" so it was randomly generated
// See Nordic App Note nAN-36_v.1.1.pdf for additional details:
// Paste from Powercast's Dell Silver Laptop's nRfGo Studio: BCB8xxxx-38B1-AC2A-1F49-0C15AF33EE37 >>> backwards below per old ble_pls.c file
// ??? Above lbs example code's BASE UUID as Service's short UUID in bytes 7 & 8, but from "xxxx" above (backwards) it looks like bytes 13 & 14???
#define FLAGS_UUID_BASE {0x37, 0xEE, 0x33, 0xAF, 0x15, 0x0C, 0x49, 0x1F, 0x2A, 0xAC, 0xB1, 0x38, 0x23, 0x15, 0xB8, 0xBC}
#define FLAGS_UUID_SERVICE 0x1523
#define FLAGS_UUID_FLAG_CHAR 0x1525
#ifdef CONNECTABLE_INCLUDES_BUTTON_NOTIFICATION_LOGIC
#define FLAGS_UUID_BUTTON_CHAR 0x1524
#endif


// Forward declaration of the ble_flags_t type. 
typedef struct ble_flags_s ble_flags_t;

typedef void (*ble_flags_flag_write_handler_t) (ble_flags_t * p_flags, uint8_t new_state);

typedef struct
{
    ble_flags_flag_write_handler_t flag_write_handler;                    /**< Event handler to be called when FLAGS characteristic is written. */
} ble_flags_init_t;

/**@brief FLAGS Service structure. This contains various status information for the service. */
typedef struct ble_flags_s
{
    uint16_t                    service_handle;
    ble_gatts_char_handles_t    flag_char_handles;
	
#ifdef CONNECTABLE_INCLUDES_BUTTON_NOTIFICATION_LOGIC	
    ble_gatts_char_handles_t    button_char_handles;
#endif
	
    uint8_t                     uuid_type;
    uint16_t                    conn_handle;
    ble_flags_flag_write_handler_t flag_write_handler;
} ble_flags_t;

/**@brief Function for initializing the FLAGS Service.
 *
 * @param[out]  p_flags     FLAGS Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_flags_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_flags_init(ble_flags_t * p_flags, const ble_flags_init_t * p_flags_init);

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the FLAGS Service.
 *
 *
 * @param[in]   p_flags    FLAGS Button Service structure.
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 */
void ble_flags_on_ble_evt(ble_flags_t * p_flags, ble_evt_t * p_ble_evt);

#ifdef CONNECTABLE_INCLUDES_BUTTON_NOTIFICATION_LOGIC

/**@brief Function for sending a button state notification.
 */
uint32_t ble_flags_on_button_change(ble_flags_t * p_flags, uint8_t button_state);

#endif

#endif // BLE_FLAGS_H__

/** @} */
